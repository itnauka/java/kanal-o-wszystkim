<div align="center">
  <a href="https://www.youtube.com/watch?v=Q_4XRJuJTBM&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7">
    <img src="https://i.ytimg.com/vi/Q_4XRJuJTBM/maxresdefault.jpg" width="600px" alt="Kanał o Wszystkim" />
  </a>
<h1>Kanał o Wszystkim</h1>
</div>

## Informacje o Kursie

- **Nazwa Kursu:** Kurs Java dla początkujących - Kurs 2.0
- **Autor/Kreator:** Kanał o Wszystkim
- **Opis Kursu:** Kurs Java 2.0 dla początkujących obejmujący podstawowe zagadnienia związane z programowaniem w tej technologii.
- **Link do Kursu:** [Kurs Java dla początkujących - Kurs Java 2.0](https://www.youtube.com/playlist?list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7)

## Struktura Kursu

[#1 - Pierwszy program "Hello World"](src/main/java/org/kow/Lekcja01/README.md)  
[#2 - Typy danych, zmienne i stałe](src/main/java/org/kow/Lekcja02/README.md)  
[#3 - Operatory arytmetyczne](src/main/java/org/kow/Lekcja03/README.md)  
[#4 - Operatory porównania i logiczne](src/main/java/org/kow/Lekcja04/README.md)  
[#5 - Instrukcje warunkowe IF](src/main/java/org/kow/Lekcja05/README.md)  
[#6 - Instrukcje Switch](src/main/java/org/kow/Lekcja06/README.md)  
[#7 - Pętle](src/main/java/org/kow/Lekcja07/README.md)  
[#8 - Piszemy prostą grę](src/main/java/org/kow/Lekcja08/README.md)  
[#9 - Tablice](src/main/java/org/kow/Lekcja09/README.md)  
[#10 - Pętla "For Each"](src/main/java/org/kow/Lekcja10/README.md)  
[#11 - Funkcje / Metody](src/main/java/org/kow/Lekcja11/README.md)  
[#12 - Wysyłanie argumentów do funkcji](src/main/java/org/kow/Lekcja12/README.md)  
[#13 - Rzutowanie i konwersja typów danych](src/main/java/org/kow/Lekcja13/README.md)  
[#14 - Wstęp do klas i obiektów](src/main/java/org/kow/Lekcja14/README.md)  
[#15 - Konstruktory klas](src/main/java/org/kow/Lekcja15/README.md)  
[#16 - Klasy i obiekty, kopiowanie](src/main/java/org/kow/Lekcja16/README.md)  
[#17 - Gettery i Settery hermetyzacja danych](src/main/java/org/kow/Lekcja17/README.md)  
[#18 - Klasa Math](src/main/java/org/kow/Lekcja18/README.md)  
[#19 - Kolekcje: Lista](src/main/java/org/kow/Lekcja19/README.md)  
[#20 - Kolekcje: Map, Set](src/main/java/org/kow/Lekcja20/README.md)  
[#21 - Kolekcje: operacje, sortowanie](src/main/java/org/kow/Lekcja21/README.md)  
[#22 - Kolekcje: Kolejka, Stos](src/main/java/org/kow/Lekcja22/README.md)  
[#23 - Dziedziczenie cz. 1](src/main/java/org/kow/Lekcja23/README.md)  
[#24 - Dziedziczenie cz. 2 (Konstruktory)](src/main/java/org/kow/Lekcja24/README.md)  
[#25 - Dziedziczenie cz. 3 (Metody, abstract)](src/main/java/org/kow/Lekcja25/README.md)  
[#26 - Wyjątki (try, catch), instanceof](src/main/java/org/kow/Lekcja26/README.md)  
[#27 - Interfejsy](src/main/java/org/kow/Lekcja27/README.md)  
[#28 - Typ Enum](src/main/java/org/kow/Lekcja28/README.md)  
[#29 - Klasy anonimowe i wewnętrzne](src/main/java/org/kow/Lekcja29/README.md)  
[#30 - Wielowątkowość - Thread](src/main/java/org/kow/Lekcja30/README.md)  
[#31 - Equals vs ==](src/main/java/org/kow/Lekcja31/README.md)  
[#32 - StringBuilder - optymalizacja](src/main/java/org/kow/Lekcja32/README.md)  
[#33 - Zapis do pliku (File)](src/main/java/org/kow/Lekcja33/README.md)  
[#34 - Rekurencja](src/main/java/org/kow/Lekcja34/README.md)  

## Wymagania

- [www.replit.com](https://replit.com/)

lub  
- IDE ([Intellij](https://www.jetbrains.com/idea/download/), [VSCode](https://code.visualstudio.com/), [Eclipse](https://www.eclipse.org/downloads/packages/installer), itp.)

## Instrukcje dla Uczestników

Kurs jest przeznaczony głównie dla osób początkujących, jest łatwy do zrozumienia i skupia się na podstawowych zagadnieniach związanych z językiem 'Java'.

## Dodatkowe informacje

Cały kurs jest płatny i dostępny na stronie  
[Kurs JAVA dla zielonych](https://strefakursow.pl/kursy/programowanie/kurs_java_dla_zielonych.html?ref=87991)

## Licencja

Cały kurs podstawowy jest udostępniony na platformie YouTube i można swobodnie korzystać z materiałów.

## Kontakt

- E-mail: [jackoski@gmail.com](mailto:jackoski@gmail.com)
