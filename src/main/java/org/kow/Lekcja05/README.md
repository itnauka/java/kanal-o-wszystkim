<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#5 - Instrukcje warunkowe IF](https://www.youtube.com/watch?v=CnG7wcxYbwk&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=6)

```java
public class Main {
    public static void main(String[] args) {

        if (true) {
            System.out.println("Prawda");
        }
        if (false)
            System.out.println("Fałsz");
            System.out.println("Fałsz2");//poza blokiem if, jeżeli if jest bez {}
```  

> **Prawda**  
> **Fałsz2**  

<br />

```java
public class Main {
    public static void main(String[] args) {

        if (true) {
            System.out.println("Prawda");
        }
        if (false) {
            System.out.println("Fałsz");
            System.out.println("Fałsz2");//poza blokiem if, jeżeli if jest bez {}
        }
```  

> **Prawda**

<br />

```java
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj swój wiek: ");
        int wiek = scanner.nextInt();

        if (wiek >= 18) {
            System.out.println("Pełnoletnia");
        } else {
            if (wiek >= 16) {
                System.out.println("Możesz pracować");
            } else {
                System.out.println("Nie możesz nic!");
            }
        }
    }
}
```  

> **Podaj swój wiek: <span style="color:green;">16</span>**  
> **Możesz pracować**  

> **Podaj swój wiek: <span style="color:green;">13</span>**  
> **Nie możesz nic!**  

> **Podaj swój wiek: <span style="color:green;">18</span>**  
> **Pełnoletnia**  

<br />

```java
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj swój wiek: ");
        int wiek = scanner.nextInt();

        // wykonuje się tylko jeden warunek
        if (wiek >= 18) {
            System.out.println("Pełnoletnia");
        } else if (wiek >= 16) {
            System.out.println("Możesz pracować");
        } else {
            System.out.println("Nie możesz nic!");
        }
    }
}
```  

> **Podaj swój wiek: <span style="color:green;">16</span>**  
> **Możesz pracować**

> **Podaj swój wiek: <span style="color:green;">13</span>**  
> **Nie możesz nic!**

> **Podaj swój wiek: <span style="color:green;">18</span>**  
> **Pełnoletnia**

<br />

```java
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj swój wiek: ");
        int wiek = scanner.nextInt();

        //Operator trój argumentowy
        String czyPelnoletni = wiek >= 18 ? "Pełnoletni" : "NIE pełnoletni";
        System.out.println("Czy jest pełnoletni ?: " + czyPelnoletni);
    }
}
```  
> **Podaj swój wiek: <span style="color:green;">16</span>**  
> **Czy jest pełnoletni ?: NIE pełnoletni**  

<br />


