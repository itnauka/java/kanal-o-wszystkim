//5. Kurs Java dla początkujących - Instrukcje warunkowe IF
//https://www.youtube.com/watch?v=CnG7wcxYbwk&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=6

package org.kow.Lekcja05;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

//        if (true) {
//            System.out.println("Prawda");
//        }
//        if (false) {
//            System.out.println("Fałsz");
//            System.out.println("Fałsz");//poza blokiem if, jeżeli if jest bez {}
//        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj swój wiek: ");
        int wiek = scanner.nextInt();

//        if (wiek >= 18) {
//            System.out.println("Pełnoletnia");
//        } else {
//            if (wiek >= 16) {
//                System.out.println("Możesz pracować");
//            } else {
//                System.out.println("Nie możesz nic!");
//            }
//        }

        // wykonuje się tylko jeden warunek
        if (wiek >= 18) {
            System.out.println("Pełnoletnia");
        } else if (wiek >= 16) {
            System.out.println("Możesz pracować");
        } else {
            System.out.println("Nie możesz nic!");
        }

        //Operator trój argumentowy
        String czyPelnoletni = wiek >= 18 ? "Pełnoletni" : "NIE pełnoletni";
        System.out.println("Czy jest pełnoletni ?: " + czyPelnoletni);
    }
}
