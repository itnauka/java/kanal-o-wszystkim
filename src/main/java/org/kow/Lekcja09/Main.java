//9. Kurs Java dla początkujących - Tablice
//https://www.youtube.com/watch?v=H7IHjFckX5A&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=10

package org.kow.Lekcja09;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[] tabString = new String[20];
        //tab[20] = "Zosia";//Błąd index od 0 do 19

        tabString[0] = "Ania";
        tabString[1] = "Bartek";
        tabString[2] = "Adam";
        tabString[3] = "Jacek";
        tabString[4] = "Grażyna";


        System.out.println("Długość tablicy: " + tabString.length);
        System.out.println("Druga wartość w tablicy to: " + tabString[3]);

        for (int i = 0; i < tabString.length; i++) {
            if (tabString[i] == null){
                continue;
            }
            System.out.println("Nr indeksu: " + i + ", wartość: " + tabString[i]);
        }

        System.out.println("============");

        //int[] tabInteger = {3, 0, 6, 9, 10, 57, 201};//tablica 7 elementowa
        int[] tabInteger = new int[8];
        tabInteger[0] = 3;
        tabInteger[1] = 0;
        tabInteger[2] = 6;
        tabInteger[3] = 9;

        for (int i = 0; i < tabInteger.length; i++) {
            System.out.println("Nr indeksu: " + i + ", wartość: " + tabInteger[i]);
        }


    }
}
