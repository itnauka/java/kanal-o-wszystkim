<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#9 - Tablice](https://www.youtube.com/watch?v=H7IHjFckX5A&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=10)

```java
public class Main {
    public static void main(String[] args) {
        String[] tabString = new String[20];
        tabString[0] = "Ania";
        tabString[1] = "Bartek";
        tabString[2] = "Adam";
        tabString[3] = "Jacek";
        tabString[4] = "Grażyna";
        //tab[20] = "Zosia";//Błąd index od 0 do 19

        System.out.println("Długość tablicy: " + tabString.length);
        System.out.println("Druga wartość w tablicy to: " + tabString[3]);

        for (int i = 0; i < tabString.length; i++) {
            if (tabString[i] == null){
                continue;
            }
            System.out.println("Nr indeksu: " + i + ", wartość: " + tabString[i]);
        }
    }
}
```  

> **Długość tablicy: 20**  
> **Druga wartość w tablicy to: Jacek**  
> **Nr indeksu: 0, wartość: Ania**  
> **Nr indeksu: 1, wartość: Bartek**  
> **Nr indeksu: 2, wartość: Adam**  
> **Nr indeksu: 3, wartość: Jacek**  
> **Nr indeksu: 4, wartość: Grażyna**  

<br />

```java
public class Main {
    public static void main(String[] args) {

        //int[] tabInteger = {3, 0, 6, 9, 10, 57, 201};//tablica 7 elementowa
        int[] tabInteger = new int[8];
        tabInteger[0] = 3;
        tabInteger[1] = 0;
        tabInteger[2] = 6;
        tabInteger[3] = 9;

        for (int i = 0; i < tabInteger.length; i++) {
            System.out.println("Nr indeksu: " + i + ", wartość: " + tabInteger[i]);
        }
    }
}
```  

> **Nr indeksu: 0, wartość: 3**  
> **Nr indeksu: 1, wartość: 0**  
> **Nr indeksu: 2, wartość: 6**  
> **Nr indeksu: 3, wartość: 9**  
> **Nr indeksu: 4, wartość: 0**  
> **Nr indeksu: 5, wartość: 0**  
> **Nr indeksu: 6, wartość: 0**  
> **Nr indeksu: 7, wartość: 0**  

<br />




