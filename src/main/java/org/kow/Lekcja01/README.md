<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

___

### [#1 - Pierwszy program "Hello World"](https://www.youtube.com/watch?v=T3Pla6wZd4E&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=2)  

```java
public static void main(String[] args) {
    System.out.println("Hello World!");
}
```  

>**Hello World!**  

<br />

```java
public class Main {
public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String name;

        System.out.print("Podaj swoje imie i nazwisko: ");
        name = sc.next();//działa do pierwszej napotkanej spacji
        System.out.println("Hello " + name);
        sc.close();
    }
}
```

>**Podaj swoje imie i nazwisko: <span style="color:green">Jan Kowalski</span>**  
>**Hello Jan**  
 
<br />

```java
public class Main {
public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String name;

        System.out.print("Podaj swoje imie i nazwisko: ");
        name = sc.next();
        System.out.println("Hello " + name);
        sc.nextLine();//

        System.out.print("Podaj swoje imie i nazwisko: ");
        name = sc.nextLine();
        System.out.println("Hello " + name);
        
        sc.close();
    }
}
```  

> **Podaj swoje imie i nazwisko: <span style="color:green">Jan Kowalski</span>**  
> **Hello Jan**  
> **Podaj swoje imie i nazwisko: <span style="color:green">Jan Kowalski</span>**  
> **Hello Jan Kowalski**  

<br />  

```java
public class Main {
public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String name;

        System.out.print("Podaj swoje imie i nazwisko: ");
        name = sc.nextLine();
        System.out.println("Hello " + name);
        System.out.printf("Hello %s ", name);
        sc.close();
    }
}
```  

> **Podaj swoje imie i nazwisko: <span style="color:green">Jan Kowalski</span>**  
> **Hello Jan Kowalski**  
> **Hello Jan Kowalski**  

