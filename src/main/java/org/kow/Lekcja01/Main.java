//1. Kurs Java dla początkujących - Pierwszy program "Hello World"
//https://www.youtube.com/watch?v=T3Pla6wZd4E&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=2

package org.kow.Lekcja01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String name;

        System.out.print("Podaj swoje imie i nazwisko: ");
        name = sc.next();
        System.out.println("Hello " + name);
        sc.nextLine();

        System.out.print("Podaj swoje imie i nazwisko: ");
        name = sc.nextLine();
        System.out.println("Hello " + name);

        System.out.print("Podaj swoje imie i nazwisko: ");
        name = sc.nextLine();
        System.out.println("Hello " + name);
        System.out.printf("Hello %s ", name);
        sc.close();



    }
}
