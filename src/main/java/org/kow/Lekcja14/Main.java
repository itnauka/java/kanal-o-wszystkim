//14. Kurs Java dla początkujących - Wstęp do klas i obiektów
//https://www.youtube.com/watch?v=y0EvXmqnkM8&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=15

package org.kow.Lekcja14;

public class Main {
    public static void main(String[] args) {

        Zwierze kot = new Zwierze();
        System.out.println(Zwierze.liczebnosc);
        kot.glos = "Meeeeow";
        kot.imie = "Milka";
        System.out.println(kot.imie);
        kot.przedstawSie();
        kot.dajGlos(3);

        Zwierze pies = new Zwierze();
        System.out.println(Zwierze.liczebnosc);
        pies.imie = "Reksio";
        pies.przedstawSie();
        pies.dajGlos(4);

    }
}

class Jacek {
    int wzrost;
    int waga;
    String kolorOczu;
    
}
