package org.kow.Lekcja14;

public class Zwierze {

    static int liczebnosc;
    String imie;
    String glos = "Grrrrrr";

    Zwierze() {
        liczebnosc++;
    }

    public void dajGlos(int x) {
        for (int i = 0; i < x; i++) {
            System.out.print(glos);
            System.out.print(" ");
        }
        System.out.println();
    }

    public void przedstawSie() {
        System.out.println("Nazywam się " + imie);
    }

}
