<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#14 - Wstęp do klas i obiektów](https://www.youtube.com/watch?v=y0EvXmqnkM8&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=16)

##### Klasa `Zwierze`  

```java
public class Zwierze {

    static int liczebnosc;
    String imie;
    String glos = "Grrrrrr";

    Zwierze() {
        liczebnosc++;
    }

    public void dajGlos(int x) {
        for (int i = 0; i < x; i++) {
            System.out.print(glos);
            System.out.print(" ");
        }
        System.out.println();
    }

    public void przedstawSie() {
        System.out.println("Nazywam się " + imie);
    }

}
```

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        
        Zwierze kot = new Zwierze();
        System.out.println(Zwierze.liczebnosc);
        kot.glos = "Meeeeow";
        kot.imie = "Milka";
        System.out.println(kot.imie);
        kot.przedstawSie();
        kot.dajGlos(3);
        
        Zwierze pies = new Zwierze();
        System.out.println(Zwierze.liczebnosc);
        pies.imie = "Reksio";
        pies.przedstawSie();
        pies.dajGlos(4);
    }
}
```  

> **1**  
> **Milka**  
> **Nazywam się Milka**  
> **Meeeeow Meeeeow Meeeeow**  
> **2**  
> **Nazywam się Reksio**  
> **Grrrrrr Grrrrrr Grrrrrr Grrrrrr**  

<br>





