//7. Kurs Java dla początkujących - Pętle
//https://www.youtube.com/watch?v=pv8at_MjMbU&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=8

package org.kow.Lekcja07;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int i = 5;

        //Pętla while
        while (i > 0) {
            System.out.println(i);
            i--;
        }
        System.out.println(i);

        System.out.println("------");

        //Pętla do-while
        i = -5;
        do {
            System.out.println(i);
            i--;
        } while (i > 0);

        System.out.println("=========");

        //Pętla for
        for (i = 5; i > 0; i--) {
            System.out.println(i);
        }

        System.out.println("------");

        i = 5;
        for (; i > 0; i--) {
            System.out.println(i);
        }

    }
}
