<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#7 - Pętle](https://www.youtube.com/watch?v=pv8at_MjMbU&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=8)

##### Pętla while  

```java
public class Main {
    public static void main(String[] args) {
        
        //Pętla while
        int i = 5;
        while (i > 0) {
            System.out.println(i);
            i--;
        }
        System.out.println(i);
    }
}
```  

> **5**  
> **4**  
> **3**  
> **2**  
> **1**  
> **0**  

<br />

##### Pętla do-while

```java
public class Main {
    public static void main(String[] args) {
        
        //Pętla do-while
        i = -5;
        do {
            System.out.println(i);
            i--;
        } while (i > 0);
    }
}
```  

> **-5**  

<br />

##### Pętla for

```java
public class Main {
    public static void main(String[] args) {

        //Pętla for
        for (i = 5; i > 0; i--) {
            System.out.println(i);
        }

        System.out.println("------");

        i = 5;
        for (; i > 0; i--) {
            System.out.println(i);
        }
        System.out.println(i);
    }
}
```  

> **5**  
> **4**  
> **3**  
> **2**  
> **1**  
> **------**  
> **5**  
> **4**  
> **3**  
> **2**  
> **1**  
> **0**  

<br />





