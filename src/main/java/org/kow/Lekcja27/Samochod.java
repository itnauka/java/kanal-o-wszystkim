package org.kow.Lekcja27;

public class Samochod extends Pojazd implements Poruszanie, Comparable<String> {
    int iloscKol;

    public Samochod(String nazwa, int iloscKol) {
        super(nazwa);
        this.iloscKol = iloscKol;
    }

    @Override
    public void jedzDoPrzodu() {
        System.out.println("Jadę w klasie Samochod");
    }

    @Override
    public void skrecaj() {
        System.out.println("skręcam w klasie Samochod");
    }

    @Override
    public int compareTo(String o) {
        return 0;
    }
}
