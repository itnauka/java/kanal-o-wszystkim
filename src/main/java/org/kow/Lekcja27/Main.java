//27. Kurs Java dla początkujących - Interfejsy
//https://www.youtube.com/watch?v=7g4FIK5GG4Q&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=28

package org.kow.Lekcja27;

public class Main {
    public static void main(String[] args) {
        System.out.println(Poruszanie.NAZWA_INTERFEJSU);

        Samochod car = new Samochod("BMW", 4);
        car.skrecaj();
        car.jedzDoPrzodu();
    }
}
