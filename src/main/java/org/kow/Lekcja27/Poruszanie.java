package org.kow.Lekcja27;

public interface Poruszanie {
    String NAZWA_INTERFEJSU = "Poruszanie";

    void jedzDoPrzodu();
    void skrecaj();
}
