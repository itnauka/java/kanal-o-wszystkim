<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#27 - Interfejsy](https://www.youtube.com/watch?v=7g4FIK5GG4Q&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=28)

##### Klasa `Pojazd`

```java
public class Pojazd {
    String name;
    
    Pojazd(String name) {
        this.name = name;
    }
}
```

<br>

##### Klasa `Samochod`

```java
public class Samochod extends Pojazd implements Poruszanie, Comparable<String> {
    int iloscKol;
    
    public Samochod(String nazwa, int iloscKol) {
        super(nazwa);
        this.iloscKol = iloscKol;
    }
    
    @Override
    public void jedzDoPrzodu() {
        System.out.println("Jadę w klasie Samochod");
    }
    
    @Override
    public void skrecaj() {
        System.out.println("skręcam w klasie Samochod");
    }
    
    @Override
    public int compareTo(String o) {
        return 0;
    }
}
```

<br>

##### Interfejs `Poruszanie`

```java
public interface Poruszanie {
    String NAZWA_INTERFEJSU = "Poruszanie";
    
    void jedzDoPrzodu();
    void skrecaj();
}
```

<br>

##### Interfejs `InnyInterface`

```java
public interface InnyInterface {
    void metodyInnegoInterfejsu();
}
```  

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        System.out.println(Poruszanie.NAZWA_INTERFEJSU);
        Samochod car = new Samochod("BMW", 4);
        car.skrecaj();
        car.jedzDoPrzodu();
    }
}
```  

> **Poruszanie**  
> **skręcam w klasie Samochod**  
> **Jadę w klasie Samochod**  

<br>