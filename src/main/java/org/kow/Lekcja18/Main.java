//18. Kurs Java dla początkujących - Klasa Math
//https://www.youtube.com/watch?v=PApXrezKacg&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=19

package org.kow.Lekcja18;

public class Main {
    public static void main(String[] args) {

        int a = 5;
        int b = 11;
        int c = -27;

        //Math
        System.out.println("Maximum: " + Math.max(a, b));
        System.out.println("Minimum: " + Math.min(a, b));
        System.out.println("Wartość bezwzględna: " + Math.abs(c));
        System.out.println("Potęgowanie: " + Math.pow(2, 3));//zwraca double
        System.out.println("Pierwiastek: " + Math.sqrt(16));
        System.out.println("Pierwiastek 3 stopnia: " + Math.pow(27, 1.0/3.0));
        System.out.println("Zaokrąglanie: " + Math.round(5.4));
        System.out.println("Zaokrąglanie: " + Math.ceil(5.1110));//Zaokrąglanie w górę
        System.out.println("Zaokrąglanie: " + Math.floor(5.99));//Zaokrąglanie w dół
        System.out.println("Wartość PI: " + Math.PI);
        System.out.println("Wartość E: " + Math.E);

        System.out.println("-----");

        for (int i = 0; i < 10; i++) {
            System.out.println((int) (Math.random() * 10) + 1);//od 0.0 do 1.0
        }
    }
}
