<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#18 - Klasa Math](https://www.youtube.com/watch?v=PApXrezKacg&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=19)

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        
        int a = 5;
        int b = 11;
        int c = -27;
        
        //Math
        System.out.println("Maximum: " + Math.max(a, b));
        System.out.println("Minimum: " + Math.min(a, b));
        System.out.println("Wartość bezwzględna: " + Math.abs(c));
        System.out.println("Potęgowanie: " + Math.pow(2, 3));//zwraca double
        System.out.println("Pierwiastek: " + Math.sqrt(16));
        System.out.println("Pierwiastek 3 stopnia: " + Math.pow(8, 1.0/3.0));//zwraca double
        System.out.println("Zaokrąglanie: " + Math.round(5.4));
        System.out.println("Zaokrąglanie: " + Math.ceil(5.1110));
        System.out.println("Zaokrąglanie: " + Math.floor(5.99));
        System.out.println("Wartość PI: " + Math.PI);
        System.out.println("Wartość E: " + Math.E);
    }
}
```  

> **Maximum: 11**  
> **Minimum: 5**  
> **Wartość bezwzględna: 27**  
> **Potęgowanie: 8.0**  
> **Pierwiastek: 4.0**  
> **Pierwiastek 3 stopnia: 2.0**  
> **Zaokrąglanie: 5**  
> **Zaokrąglanie: 6.0**  
> **Zaokrąglanie: 5.0**  
> **Wartość PI: 3.141592653589793**  
> **Wartość E: 2.718281828459045**  

<br>

```java
public class Main {
    public static void main(String[] args) {
        
        for (int i = 0; i < 10; i++) {
            System.out.println((int) (Math.random() * 10) + 1);//od 0.0 do 1.0
        }
    }
}
```  

> **4**  
> **10**  
> **8**  
> **9**  
> **4**  
> **4**  
> **5**  
> **5**  
> **1**  
> **10**  

<br>





