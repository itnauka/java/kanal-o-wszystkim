<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#29 - Klasy anonimowe i wewnętrzne](https://www.youtube.com/watch?v=CemXLFn298Q&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=30)

##### Klasa `Animal`

```java
public class Animal {
    void makeVoice() {
        System.out.println("Klasa Animal:makeVoice(): Grrrrr!!!");
    }
    
    void eat() {
        System.out.println("Klasa Animal:eat(): MNIAM MNIAM MNIAM");
    }
    
    class Pet {
        String name;
        
        void getName() {
            //makeVoice();//private
            System.out.println("Klasa wewnętrzna Animal:Pet:getName(): " + name);
            System.out.println("Ma dostęp do prywatnych zmiennych i metod klasy Animal");
        }
    }
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    
    public static void main(String[] args) {
        Animal cat = new Animal() {
            @Override
            void makeVoice() {
                super.makeVoice();//Grrrrr!!!
            }
        };
        
        cat.makeVoice();
    }
}
```  

> **Klasa Animal:makeVoice(): Grrrrr!!!**  

<br>

##### Klasa `Main`

```java
public class Main {
    
    public static void main(String[] args) {
        Animal cat = new Animal() {
            @Override
            void makeVoice() {
                System.out.println("Klasa anonimowa Animal:makeVoice(): Meowww, Meowww");
            }
        };
        
        cat.makeVoice();
    }
}
```  

> **Klasa anonimowa Animal:makeVoice(): Meowww, Meowww**

<br>

##### Klasa `Main`

```java
public class Main {
    
    public static void main(String[] args) {
        Animal cat = new Animal() {
            @Override
            void makeVoice() {
                //super.makeVoice();//Grrrrr!!!
                System.out.println("Klasa anonimowa Animal:makeVoice(): Meowww, Meowww");
            }
        };
        cat.makeVoice();
        
        Animal cat2 = new Animal();
        cat2.makeVoice();
    }
}
```  

> **Klasa anonimowa Animal:makeVoice(): Meowww, Meowww**  
> **Klasa Animal:makeVoice(): Grrrrr!!!**  

<br>

##### Klasa `Main`

```java
public class Main {
    
    public static void main(String[] args) {
        Animal cat = new Animal() {
            @Override
            void makeVoice() {
                //super.makeVoice();//Grrrrr!!!
                System.out.println("Klasa anonimowa Animal:makeVoice(): Meowww, Meowww");
            }
        };
        cat.makeVoice();
        
        Animal cat2 = new Animal();
        cat2.makeVoice();
        
        System.out.println("----");
        
        //'cat' oraz 'cat1' mają tę samą metodę 'eat()'
        cat.eat();//cat
        cat2.eat();
    }
}
```  

> **Klasa anonimowa Animal:makeVoice(): Meowww, Meowww**  
> **Klasa Animal:makeVoice(): Grrrrr!!!**  
> **----**  
> **Klasa Animal:eat(): MNIAM MNIAM MNIAM**  
> **Klasa Animal:eat(): MNIAM MNIAM MNIAM**  

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        Animal cat = new Animal() {
            @Override
            void makeVoice() {
                System.out.println("Klasa anonimowa Animal:makeVoice(): Meowww, Meowww");
            }
        };
        
        //Tworzenie obiektu na bazie klasy wewnętrznej 'Pet'
        Animal.Pet pet = cat.new Pet();//Wywołanie obiektu 'pet' na obiekcie 'kat' klasy 'Animal'
        pet.name = "NUKA";
        pet.getName();
    }
}
```  

> **Klasa wewnętrzna Animal:Pet:getName(): NUKA**  
> **Ma dostęp do prywatnych zmiennych i metod klasy Animal**  

<br>

