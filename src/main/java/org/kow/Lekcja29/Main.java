//29. Kurs Java dla początkujących - Klasy anonimowe i wewnętrzne
//https://www.youtube.com/watch?v=CemXLFn298Q&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=30

package org.kow.Lekcja29;

public class Main {

    public static void main(String[] args) {
        Animal cat = new Animal() {
            @Override
            void makeVoice() {
                //super.makeVoice();//Grrrrr!!!
                System.out.println("Klasa anonimowa Animal:makeVoice(): Meowww, Meowww");
            }
        };

        cat.makeVoice();

        Animal cat2 = new Animal();
        cat2.makeVoice();

        System.out.println("----");

        cat.eat();
        cat2.eat();

        System.out.println("===++++++++===");

        Animal.Pet pet = cat.new Pet();//Wywołanie obiektu 'pet' na obiekcie 'kat' klasy 'Animal'
        pet.name = "NUKA";
        pet.getName();

    }
}
