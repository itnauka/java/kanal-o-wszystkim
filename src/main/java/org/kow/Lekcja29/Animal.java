package org.kow.Lekcja29;

public class Animal {
    void makeVoice() {
        System.out.println("Klasa Animal:makeVoice(): Grrrrr!!!");
    }

    void eat() {
        System.out.println("Klasa Animal:eat(): MNIAM MNIAM MNIAM");
    }

    //Klasa wewnętrzna 'Pet' w klasie 'Animial'
    class Pet {
        String name;

        void getName() {
            //makeVoice();//private
            System.out.println("Klasa wewnętrzna Animal:Pet:getName(): " + name);
            System.out.println("Ma dostęp do prywatnych zmiennych i metod klasy Animal");
        }
    }
}
