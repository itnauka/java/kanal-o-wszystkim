<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#3 - Operatory arytmetyczne](https://www.youtube.com/watch?v=SjPYpqWZxVY&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=4)

```java
public class Main {
public static void main(String[] args) {
    int a, b, wynik;

    a = 25;
    b = 10;
    wynik = 25 + b;
    System.out.println("Wynik dodawania: " + wynik);

    wynik = a - b;
    System.out.println("Wynik odejmowania: " + wynik);

    wynik = a * b;
    System.out.println("Wynik mnożenia: " + wynik);

    wynik = a / b;
    System.out.println("Wynik dzielenia: " + wynik);//2 ponieważ int
    double wynikDouble = (double) a / b;// rzutowanie 
    System.out.println("Wynik dzielenia float: " + wynikDouble);

    wynik = a % b;
    System.out.println("Wynik \'modulo\': " + wynik + " reszty z dzielenia");
    }
}
```  

> **Wynik dodawania: 35**  
> **Wynik odejmowania: 15**  
> **Wynik mnożenia: 250**  
> **Wynik dzielenia: 2**  
> **Wynik dzielenia float: 2.5**  
> **Wynik 'modulo': 5 reszty z dzielenia**  

<br />

```java
public class Main {
    public static void main(String[] args) {
        int a, b, wynik;

        a = 25;
        b = 0;

        wynik = a / b;//NIE dzielimy przez 0 'zero'
        System.out.println("Wynik dzielenia: " + wynik);
    }
}
```  

> **Exception in thread "main" java.lang.ArithmeticException: / by zero**  
> **at org.kow.Lekcja03.Main.main(Main.java:22)**

<br />

```java
public class Main {
public static void main(String[] args) {

    int a, b, wynik;

    //Kolejność działań
    wynik = 2 + 2 * 2;//2 + (2 * 2)
    System.out.println("wynik = " + wynik);
    
    a = 25;
    b = 10;
    
    //Skrócone operatory
    a += b;
    System.out.println("Dodawanie do a: " + a);
    a -= b;
    System.out.println("Odejmowanie do a: " + a);
    a *= b;
    System.out.println("Mnożenie do a: " + a);
    a /= b;
    System.out.println("Dzielenie do a: " + a);
    a %= b;
    System.out.println("Modulo do a: " + a);

    System.out.println("===================");

    //Potęgowanie
    System.out.println("Potęgowanie: " + Math.pow(2, 3));//

    //Pierwiastkowanie
    System.out.println("Pierwiastkowanie: " + Math.sqrt(9));

    //Wartość bezwzględa
    System.out.println("Wartość bezwzględna: " + Math.abs(-359));

    //Liczba PI
    System.out.println("LIczba PI: " + Math.PI);
    }
}
```  

> **wynik = 6**  
> **Dodawanie do a: 35**  
> **Odejmowanie do a: 25**  
> **Mnożenie do a: 250**  
> **Dzielenie do a: 25**  
> **Modulo do a: 5**  

<br />

```java
public class Main {
public static void main(String[] args) {

    int a, b, wynik;
    
    a = 25;
    b = 10;
    
    //Potęgowanie
    System.out.println("Potęgowanie: " + Math.pow(2, 3));//

    //Pierwiastkowanie
    System.out.println("Pierwiastkowanie: " + Math.sqrt(9));

    //Wartość bezwzględa
    System.out.println("Wartość bezwzględna: " + Math.abs(-359));

    //Liczba PI
    System.out.println("Liczba PI: " + Math.PI);
    }
}
```  

> **Potęgowanie: 8.0**  
> **Pierwiastkowanie: 3.0**  
> **Wartość bezwzględna: 359**  
> **Liczba PI: 3.141592653589793**  

<br />

```java
public class Main {
public static void main(String[] args) {
    int a = 5;

    //Inkrementacja
    System.out.println("Przed inkrementacją: " + a);
    a++;// a += 1;
    System.out.println("Po inkrementacji: " + a);

    System.out.println("----");

    //Dekrementacja
    System.out.println("Przed dekrementajcą: " + a);
    a--;// a -= 1;
    System.out.println("Po dekrementacji: " + a);

    System.out.println("----");

    //Preinkrementacja i predekrementacja
    System.out.println("Wartość a: " + a);
    System.out.println("preinkrementacja: " + ++a);
    System.out.println("predekrementacja: " + --a);

    System.out.println("----");

    //Postinkrementacja i postdekrementacja
    System.out.println("Wartość a: " + a);
    System.out.println("Postinkrementacja: " + a++);
    System.out.println("Postdekrementacja: " + a--);
    System.out.println("Wartość a po wyjściu z metody println: " + a);
    }
}
```  

> **Przed inkrementacją: 5**  
> **Po inkrementacji: 6**  
> **----**  
> **Przed dekrementajcą: 6**  
> **Po dekrementacji: 5**  
> **----**  
> **Wartość a: 5**  
> **preinkrementacja: 6**  
> **predekrementacja: 5**  
> **----**  
> **Wartość a: 5**  
> **Postinkrementacja: 5**  
> **Postdekrementacja: 6**  
> **Wartość a po wyjściu z metody println: 5**  

<br />



