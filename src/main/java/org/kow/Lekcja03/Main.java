//3. Kurs Java dla początkujących - Operatory arytmetyczne
//https://www.youtube.com/watch?v=SjPYpqWZxVY&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=4

package org.kow.Lekcja03;

public class Main {
    public static void main(String[] args) {
        int a, b, wynik;

        a = 25;
        b = 10;
        wynik = a + b;
        System.out.println("Wynik dodawania: " + wynik);

        wynik = a - b;
        System.out.println("Wynik odejmowania: " + wynik);

        wynik = a * b;
        System.out.println("Wynik mnożenia: " + wynik);

        System.out.println("----");
        wynik = a / b;
        System.out.println("Wynik dzielenia: " + wynik);//2 ponieważ int
        double wynikDouble = (double) a / b;
        System.out.println("Wynik dzielenia float: " + wynikDouble);
        System.out.println("----");

        wynik = a % b;
        System.out.println("Wynik \'modulo\': " + wynik + " reszty z dzielenia");

        System.out.println("===================");

        //Kolejność działań
        wynik = 2 + 2 * 2;//2 + (2 * 2)
        System.out.println("wynik = " + wynik);

        //Skrócone operatory
        a += b;
        System.out.println("Dodawanie do a: " + a);
        a -= b;
        System.out.println("Odejmowanie do a: " + a);
        a *= b;
        System.out.println("Mnożenie do a: " + a);
        a /= b;
        System.out.println("Dzielenie do a: " + a);
        a %= b;
        System.out.println("Modulo do a: " + a);

        System.out.println("===================");

        //Potęgowanie
        System.out.println("Potęgowanie: " + Math.pow(2, 3));//

        //Pierwiastkowanie
        System.out.println("Pierwiastkowanie: " + Math.sqrt(9));

        //Wartość bezwzględa
        System.out.println("Wartość bezwzględna: " + Math.abs(-359));

        //Liczba PI
        System.out.println("Liczba PI: " + Math.PI);

        System.out.println("===================");

        //Inkrementacja
        System.out.println("Przed inkrementacją: " + a);
        a++;// a += 1;
        System.out.println("Po inkrementacji: " + a);

        System.out.println("----");

        //Dekrementacja
        System.out.println("Przed dekrementajcą: " + a);
        a--;// a -= 1;
        System.out.println("Po dekrementacji: " + a);

        System.out.println("----");

        //Preinkrementacja i predekrementacja
        System.out.println("Wartość a: " + a);
        System.out.println("preinkrementacja: " + ++a);
        System.out.println("predekrementacja: " + --a);

        System.out.println("----");

        //Postinkrementacja i postdekrementacja
        System.out.println("Wartość a: " + a);
        System.out.println("Postinkrementacja: " + a++);
        System.out.println("Postdekrementacja: " + a--);
        System.out.println("Wartość a po wyjściu z metody println: " + a);
    }
}
