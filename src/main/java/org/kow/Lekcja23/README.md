<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#23 - Dziedziczenie cz. 1](https://www.youtube.com/watch?v=9xdzH5GE4bw&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=24)

##### Klasa `Pojazd`

```java
public class Pojazd {
    String marka;
    int iloscKol;
    
    protected void odpal() {
        System.out.println("Pojazd odpalił.");
    }
}
```

<br>

##### Klasa `Samochod`

```java
public class Samochod extends Pojazd {
    boolean otwarty;
    
    void zamknijOtworz() {
        otwarty = !otwarty;
        if (otwarty) System.out.println("Otwarty");
        else System.out.println("Zamknięty");
    }
}
```

<br>

##### Klasa `SamochodSportowy`

```java
public class SamochodSportowy extends Samochod {
}
```

<br>

##### Klasa `Motocykl`

```java
public class Motocykl extends Pojazd {
    Motocykl() {
        super.iloscKol = 2;
    }
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        Samochod audi = new Samochod();
        audi.iloscKol = 4;
        audi.marka = "Audi";
        System.out.println(audi.iloscKol + " " + audi.marka);
        audi.odpal();
        audi.zamknijOtworz();//Otwarty
        audi.zamknijOtworz();//Zamknięty
        
        System.out.println("-----");
        
        SamochodSportowy subaru = new SamochodSportowy();
        subaru.odpal();
        subaru.zamknijOtworz();//Otwarty
        
        System.out.println("-----");
        
        Motocykl suzuki = new Motocykl();
        System.out.println(suzuki.iloscKol);
        //suzuki.zamknijOtworz()//nie dziedziczy po klasie Samochod
    }
}
```  

> **4 Audi**  
> **Pojazd odpalił.**  
> **Otwarty**  
> **Zamknięty**  
> **-----**  
> **Pojazd odpalił.**  
> **Otwarty**  
> **-----**  
> **2**

<br>

