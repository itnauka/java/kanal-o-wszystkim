//23. Kurs Java dla początkujących - Dziedziczenie cz. 1
//https://www.youtube.com/watch?v=9xdzH5GE4bw&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=24

package org.kow.Lekcja23;

public class Main {
    public static void main(String[] args) {
        Samochod audi = new Samochod();
        audi.iloscKol = 4;
        audi.marka = "Audi";
        System.out.println(audi.iloscKol + " " + audi.marka);
        audi.odpal();
        audi.zamknijOtworz();//Otwarty
        audi.zamknijOtworz();//Zamknięty

        System.out.println("-----");

        SamochodSportowy subaru = new SamochodSportowy();
        subaru.odpal();
        subaru.zamknijOtworz();//Otwarty

        System.out.println("-----");

        Motocykl suzuki = new Motocykl();
        System.out.println(suzuki.iloscKol);
        //suzuki.zamknijOtworz()//nie dziedziczy po klasie Samochod
    }
}
