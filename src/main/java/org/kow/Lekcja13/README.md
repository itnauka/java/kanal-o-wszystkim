<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#13 - Rzutowanie i konwersja typów danych](https://www.youtube.com/watch?v=pOPfj9tg274&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=15)

##### Typy proste  

```java
public class Main {
    public static void main(String[] args) {
        
        short a = 25;
        int b = a;//Rzutowanie niejawne
        System.out.println("Rzutowanie niejawne: " + b);
        
        int c = 25;
        short d = (short) c;//Rzutowanie jawne
        System.out.println("Rzutowanie jawne: " + d);
    }
}
```  

> **Rzutowanie niejawne: 25**  
> **Rzutowanie jawne: 25**  

<br>

```java
public class Main {
    public static void main(String[] args) {

        //Przekręcenie licznika
        int e = Integer.MAX_VALUE;
        short f = (short) e;
        
        System.out.println(f);
    }
}
```  

> **-1**  

<br>

```java
public class Main {
    public static void main(String[] args) {
        
        //Zmiennoprzecinkowe
        float a = 5.256f;
        int b = (int) a;
        
        System.out.println("Float na int: " + b);
    }
}
```  

> **Float na int: 5**  

<br>

```java
public class Main {
    public static void main(String[] args) {
        
        //Konwersja
        float a = 7.9f;
        int b = (int) a;
        String liczba = Integer.toString(b);
        System.out.println("Konwersja string na liczbe: " + liczba);
        int c = Integer.parseInt(liczba);
        System.out.println("Konwersja liczby na string: " + (c + 2));
    }
}
```  

> **Konwersja string na liczbe: 7**  
> **Konwersja liczby na string: 9**  

<br>





