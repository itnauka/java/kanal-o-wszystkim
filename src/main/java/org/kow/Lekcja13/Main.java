//13. Kurs Java dla początkujących - Rzutowanie i konwersja typów danych
//https://www.youtube.com/watch?v=pOPfj9tg274&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=14

package org.kow.Lekcja13;

public class Main {
    public static void main(String[] args) {

        short a = 25;
        int b = a;//Rzutowanie niejawne
        System.out.println("Rzutowanie niejawne: " + b);

        int c = 25;
        short d = (short) c;//Rzutowanie jawne
        System.out.println("Rzutowanie jawne: " + d);

        //Przekręcenie licznika
        int e = Integer.MAX_VALUE;
        short f = (short) e;
        System.out.println(f);

        //Zmiennoprzecinkowe
        float g = 5.256f;
        int h = (int) g;
        System.out.println("Float na int: " + h);

        //Konwersja
        float k = 7.9f;
        int l = (int) k;
        String liczba = Integer.toString(l);
        System.out.println("Konwersja string na liczbe: " + liczba);
        int m = Integer.parseInt(liczba);
        System.out.println("Konwersja liczby na string: " + (m + 2));

        System.out.println("-----");

        String string = "Podgórni";
        zmien(string);
        System.out.println(string.length());

    }
    
    private static void zmien(String s) {
        s += " Jacek";
        System.out.println("Metoda: " + s);
    }

}
