//15. Kurs Java dla początkujących - Konstruktory klas
//https://www.youtube.com/watch?v=6k8q6v57A6k&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=16

package org.kow.Lekcja15;

public class Main {
    public static void main(String[] args) {

        Czlowiek jan = new Czlowiek("Jan");
        System.out.println(Czlowiek.liczebnosc);
        //jan.imie = "Jan";

        Czlowiek tomek = new Czlowiek();
        tomek.imie = "Tomek";
        System.out.println(Czlowiek.liczebnosc);


    }
}
