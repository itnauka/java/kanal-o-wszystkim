<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#15 - Konstruktory klas](https://www.youtube.com/watch?v=6k8q6v57A6k&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=16)

##### Klasa `Czlowiek`  

```java
public class Czlowiek {
    String imie;
    static int liczebnosc = 0;
    
    Czlowiek() {
        liczebnosc++;
    }
    
    Czlowiek(String imie)  {
        liczebnosc++;
        this.imie = imie;
    }
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        
        Czlowiek jan = new Czlowiek("Jan");
        System.out.println(Czlowiek.liczebnosc);
        
        Czlowiek tomek = new Czlowiek();
        tomek.imie = "Tomek";
        System.out.println(Czlowiek.liczebnosc);
    }
}
```  

> **1**  
> **2**  

<br>





