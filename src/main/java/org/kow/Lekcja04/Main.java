//4. Kurs Java dla początkujących - Operatory porównania i logiczne
//https://www.youtube.com/watch?v=CqDaLUsykf0&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=5

package org.kow.Lekcja04;

public class Main {
    public static void main(String[] args) {

        boolean logiczna = true;
        System.out.println(logiczna);

        System.out.println("----");

        System.out.printf("int a = 5, int b = 6\n");
        int a = 5, b = 6;
        logiczna = a == b;
        System.out.println("Operator równa się = " + logiczna);
        logiczna = a != b;
        System.out.println("Operator różny od = " + logiczna);

        System.out.println("----");

        System.out.printf("int a = 7, int b = 7\n");
        a = 7;
        b = 7;
        logiczna = a == b;
        System.out.println("Operator równa się = " + logiczna);

        logiczna = a != b;
        System.out.println("Operator różny od = " + logiczna);

        System.out.println("----");

        System.out.printf("int a = 5, int b = 6\n");
        a = 5;
        b = 6;
        logiczna = a > b;
        System.out.println("operator większości = " + logiczna);
        logiczna = a < b;
        System.out.println("operator mniejszości = " + logiczna);
        logiczna = a >= b;
        System.out.println("operator większe lub równe = " + logiczna);
        logiczna = a <= b;
        System.out.println("operator mniejsze lub równe = " + logiczna);

        System.out.println("----");

        logiczna = 5 >= 5 && 6 > 1;
        System.out.println("Operator AND (&&) = " + logiczna);
        logiczna = 5 >= 5 && 6 < 1;
        System.out.println("Operator AND (&&) = " + logiczna);
        logiczna = 5 > 5 && 6 < 1;
        System.out.println("Operator AND (&&) = " + logiczna);

        System.out.println("----");

        logiczna = 5 >= 5 || 6 > 1;
        System.out.println("Operator OR (||) = " + logiczna);
        logiczna = 5 >= 5 || 6 < 1;
        System.out.println("Operator OR (||) = " + logiczna);
        logiczna = 5 > 5 || 6 < 1;
        System.out.println("Operator OR (||) = " + logiczna);

        logiczna = 10 >= 10 || true && true;// operator '&&' ma pierszeństwo przez '||'
        System.out.println(logiczna);

        System.out.println("----");

        //Negacja - operator jednoargumentowy
        logiczna = !true;//będzie 'false'
        System.out.println("Operator OR (||) = " + logiczna);
        //logiczna = !a == b;//błąd operator jednoargumentowy
        logiczna = !(a == b);
        System.out.println(logiczna);

    }
}
