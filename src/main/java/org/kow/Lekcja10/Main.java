//9. Kurs Java dla początkujących - Tablice
//https://www.youtube.com/watch?v=H7IHjFckX5A&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=10

package org.kow.Lekcja10;

public class Main {
    public static void main(String[] args) {

        String[] tabString = {"Ania", "Bartek", "Adam", "Jacek", "Grażyna"};

        //Pętla 'For'
        for (int i = 0; i < tabString.length; i++) {
            System.out.println("Nr indeksu: " + i + ", wartość: " + tabString[i]);
        }

        System.out.println("-------");

        //Pętla 'For Each'
        for (String name : tabString) {
            System.out.println("Wartość: " + name);
        }
    }
}
