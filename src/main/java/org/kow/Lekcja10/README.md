<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#10 - Pętla "For Each"](https://www.youtube.com/watch?v=CdzJ4FBzuZM&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=11)  

##### Pętla `for`  

```java
public class Main {
    public static void main(String[] args) {
        String[] tabString = {"Ania", "Bartek", "Adam", "Jacek", "Grażyna"};

        //Pętla 'For'
        for (int i = 0; i < tabString.length; i++) {
            System.out.println("Nr indeksu: " + i + ", wartość: " + tabString[i]);
        }
    }
}
```  

> **Nr indeksu: 0, wartość: Ania**  
> **Nr indeksu: 1, wartość: Bartek**  
> **Nr indeksu: 2, wartość: Adam**  
> **Nr indeksu: 3, wartość: Jacek**  
> **Nr indeksu: 4, wartość: Grażyna**  

<br />

##### Pętla `for-each`

```java
public class Main {
    public static void main(String[] args) {
        String[] tabString = {"Ania", "Bartek", "Adam", "Jacek", "Grażyna"};

        //Pętla 'For Each'
        for (String name : tabString) {
            System.out.println("Wartość: " + name);
        }
    }
}
```  

> **Wartość: Ania**  
> **Wartość: Bartek**  
> **Wartość: Adam**  
> **Wartość: Jacek**  
> **Wartość: Grażyna**  

<br />



