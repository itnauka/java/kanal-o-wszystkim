package org.kow.Lekcja26;

public class Dog extends Animal {

    String imie;

    Dog(String imie) throws ZleImieException {
        if (imie.length() == 0) {
            throw new ZleImieException();
        }
        this.imie = imie;
    }
    @Override
    public void dajGlos() {
        System.out.println("How How");
    }

    public void idz() {
        System.out.println("Pies spaceruje z Panem");
    }
}
