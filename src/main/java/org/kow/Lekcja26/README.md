<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#26 - Wyjątki (try, catch), instanceof](https://www.youtube.com/watch?v=mJD_zGajuJo&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=27)

##### Klasa `Animal`

```java
public abstract class Animal {
    abstract public void dajGlos();
}
```

<br>

##### Klasa `Dog`

```java
public class Dog extends Animal {
    
    String imie;
    
    Dog(String imie) throws ZleImieException {
        if (imie.length() == 0) {
            throw new ZleImieException();
        }
        this.imie = imie;
    }
    @Override
    public void dajGlos() {
        System.out.println("How How");
    }
    
    public void idz() {
        System.out.println("Pies spaceruje z Panem");
    }
}
```

<br>

##### Klasa `Cat`

```java
public class Cat extends Animal {
    
    @Override
    public void dajGlos() {
        System.out.println("Meow Meow");
    }
    
    public void idz() {
        System.out.println("Kot spaceruje");
    }
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        Animal kot = new Cat();
        kot.dajGlos();
        Animal pies = new Dog("");
        pies.dajGlos();
        
        ((Cat)pies).idz();
        System.out.println("Coś później");
    }
}
```  

> **Meow Meow**  
> **How How**  
> **Exception in thread "main" java.lang.ClassCastException: class org.kow.Lekcja26.
Dog cannot be cast to class org.kow.Lekcja26 at org.kow.Lekcja26.Main.main(Main.java:13)**  

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        Animal kot = new Cat();
        kot.dajGlos();
        Animal pies = new Dog("");
        pies.dajGlos();
        
        //instanceof
        System.out.println(pies instanceof Cat);//false
        System.out.println(pies instanceof Animal);//true
        System.out.println(pies instanceof Object);//true
        
        System.out.println("-----");
        
        if (pies instanceof Cat) {
            ((Cat)pies).idz();
        }
        if (pies instanceof Dog) {
            ((Dog)pies).idz();
        }
        System.out.println("Coś później");
    }
}
```  

> **Meow Meow**  
> **How How**  
> **false**  
> **true**  
> **true**  
> **-----**  
> **Pies spaceruje z Panem**  
> **Coś później**  

<br>
<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        Animal kot = new Cat();
        kot.dajGlos();
        Animal pies = new Dog("");
        pies.dajGlos();
        
        //try, catch
        try {
            ((Cat)pies).idz();
        } catch (ClassCastException ex) {
            System.out.println(ex.getMessage());
        } finally {
            //close()//Zamyka połączenie z np bazą danych
            System.out.println("Wykonuje się zawsze");
        }
        
        System.out.println("Dalsza część programu po Try-Catch");//Nie zostanie wyświetlone bez obsłużenia wyjątku!!
    }
}
```  

> **Meow Meow**  
> **How How**  
> **class org.kow.Lekcja26.Dog cannot be cast to class org.kow.Lekcja26.Cat (org.kow.Lekcja26.Dog and org.kow.Lekcja26.Cat are in unnamed module of loader 'app')**  
> **Wykonuje się zawsze**  
> **Dalsza część programu po Try-Catch**  

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        Animal kot = new Cat();
        kot.dajGlos();
        Animal pies = new Dog("");
        pies.dajGlos();
        
        Animal kot = new Cat();
        kot.dajGlos();
        Animal pies = new Dog();
        pies.dajGlos();
        
        //try, catch
        try {
            ((Dog)pies).idz();
            int a = 5 / 0;
        } catch (ClassCastException ex) {
            System.out.println(ex.getMessage());
        } catch (ArithmeticException ex){
            System.out.println(ex.getMessage());
        } finally {
            System.out.println("Wykonuje się zawsze");
        }
        System.out.println("Dalsza część programu po Try-Catch");
    }
}
```  

> **Meow Meow**  
> **How How**  
> **Pies spaceruje z Panem**  
> **/ by zero**  
> **Wykonuje się zawsze**  
> **Dalsza część programu po Try-Catch**  

<br>

##### Klasa `ZleImieException`

```java
public class ZleImieException extends Exception {
    
    @Override
    public String getMessage() {
        return "Podano błędne imię";
    }
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        Animal kot = new Cat();
        kot.dajGlos();
        
        //try, catch
        try {
            //wykona się tylko pierwszy napotkany błąd
            Animal pies = new Dog("");
            pies.dajGlos();
            
            ((Dog)pies).idz();
            int a = 5 / 0;
        } catch (ClassCastException ex) {
            System.out.println(ex.getMessage());
        } catch (ArithmeticException ex){
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println("Wyłapuje wszystkie błędy");
            System.out.println(ex.getMessage());
        } finally {
            System.out.println("Wykonuje się zawsze");
        }
        System.out.println("Dalsza część programu po Try-Catch");//Nie zostanie wyświetlone bez obsłużenia wyjątku!!
    }
}
```  

> **Meow Meow**  
> **Wyłapuje wszystkie błędy**  
> **Podano błędne imię**  
> **Wykonuje się zawsze**  
> **Dalsza część programu po Try-Catch**  

<br>

