//26. Kurs Java dla początkujących - Wyjątki (try, catch), instanceof
//https://www.youtube.com/watch?v=mJD_zGajuJo&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=27

package org.kow.Lekcja26;

public class Main {
    public static void main(String[] args) {
        System.out.println("Zmienna kot:");
        Animal kot = new Cat();
        kot.dajGlos();

/*
        System.out.println("Zmienna pies:");
        Animal pies = new Dog("");
        pies.dajGlos();

        System.out.println("-----");

        //instanceof
        System.out.println(pies instanceof Cat);//false
        System.out.println(pies instanceof Animal);//true
        System.out.println(pies instanceof Object);//true

        System.out.println("-----");

        if (pies instanceof Cat) {
            ((Cat)pies).idz();
        }
        if (pies instanceof Dog) {
            ((Dog)pies).idz();
        }

        System.out.println("Coś Później");//Nie zostanie wyświetlone bez obsłużenia wyjątku!!

        System.out.println("===++++++++++===");

        //try, catch
        try {
            ((Cat)pies).idz();
        } catch (ClassCastException ex) {
            System.out.println(ex.getMessage());
        } finally {
            //close()
            System.out.println("Wykonuje się zawsze");
        }

        System.out.println("Dalsza część programu po Try-Catch");//Nie zostanie wyświetlone bez obsłużenia wyjątku!!

        System.out.println("-----");

        try {
            ((Dog)pies).idz();
        } catch (ClassCastException ex) {
            System.out.println(ex.getMessage());
        } finally {
            //close()
            System.out.println("Wykonuje się zawsze");
        }

        System.out.println("Dalsza część programu po Try-Catch");//Nie zostanie wyświetlone bez obsłużenia wyjątku!!

        System.out.println("-----");

        try {
            ((Dog)pies).idz();
            int a = 5 / 0;
        } catch (ClassCastException ex) {
            System.out.println(ex.getMessage());
        } catch (ArithmeticException ex){
            System.out.println(ex.getMessage());
        } finally {
            //close()
            System.out.println("Wykonuje się zawsze");
        }

        System.out.println("Dalsza część programu po Try-Catch");//Nie zostanie wyświetlone bez obsłużenia wyjątku!!

 */

        System.out.println("-----");

        try {
            //wykona się tylko 1 napotkany błąd
            Animal pies = new Dog("");
            pies.dajGlos();

            ((Dog)pies).idz();
            int a = 5 / 0;
        } catch (ClassCastException ex) {
            System.out.println(ex.getMessage());
        } catch (ArithmeticException ex){
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println("Wyłapuje wszystkie błędy");
            System.out.println(ex.getMessage());
        } finally {
            //close()
            System.out.println("Wykonuje się zawsze");
        }
        System.out.println("Dalsza część programu po Try-Catch");//Nie zostanie wyświetlone bez obsłużenia wyjątku!!
    }
}
