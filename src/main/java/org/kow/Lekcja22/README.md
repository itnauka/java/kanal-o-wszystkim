<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#22 - Kolekcje: Kolejka, Stos](https://www.youtube.com/watch?v=Md7NCIMzPyI&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=23)

```java
public class Main {
    public static void main(String[] args) {
        
        //Kolejka (od pierszego do ostatniego)
        Queue<String> kolejka = new ArrayDeque<>();
        kolejka.add("Pierwszy V1");
        kolejka.add("Drugi V1");
        kolejka.add("Pierwszy");
        kolejka.add("Drugi");
        kolejka.add("Trzecie");
        kolejka.add("Czwarty");
        
        System.out.println(kolejka.size());
        
        System.out.println("-----");
        
        //Pobiera ale nie usuwa, pierwszy element kolejki, rzuca wyjątek jak kolejka jest pusta!
        System.out.println(kolejka.element());
        //Podgląda ale nie usuwa, pierwszy element kolejki, nie rzuca wyjątkiem gdy kolejka jest pusta!
        System.out.println(kolejka.peek());
        
        System.out.println("-----");
        
        //Usuwa pierwszy element kolejki, rzuca wyjątek jak kolejka jest pusta!
        System.out.println(kolejka.remove());
        //Usuwa pierwszy element kolejki, nie rzuca wyjątkiem gdy kolejka jest pusta! (zwraca 'null')
        System.out.println(kolejka.poll());
        
        System.out.println("-----");
        
        //kolejka.clear();
        
        for (String el : kolejka) {
            System.out.println(el);
        }
    }
}
```  

> **6**  
> **-----**  
> **Pierwszy V1**  
> **Pierwszy V1**  
> **-----**  
> **Pierwszy V1**  
> **Drugi V1**  
> **-----**  
> **Pierwszy**  
> **Drugi**  
> **Trzecie**  
> **Czwarty**  

<br>

```java
public class Main {
    public static void main(String[] args) {
        
        //Stos (od ostatniego do pierwszego)
        Stack<String> stos = new Stack<>();
        stos.push("Pierwszy");
        stos.push("Drugi");
        stos.push("Trzeci");
        stos.push("Czwarty");
        stos.push("Piąty");
        stos.push("Szósty");
        
        System.out.println(stos.size());
        
        System.out.println("-----");
        
        //Usuwa element ostatni
        System.out.println(stos.pop());
        //Dodaje do ostatniego elementu
        System.out.println(stos.push("Jacek Podgórni też tu jest"));
        
        System.out.println("-----");
        
        System.out.println(stos.size());
        
        System.out.println("-----");
        
        for (String el : stos) {
            System.out.println(el);
        }
    }
}
```  

> **6**  
> **-----**  
> **Szósty**  
> **Jacek Podgórni też tu jest**  
> **-----**  
> **6**  
> **-----**  
> **Pierwszy**  
> **Drugi**  
> **Trzeci**  
> **Czwarty**  
> **Piąty**  
> **Jacek Podgórni też tu jest**

<br>

### Java Collections  

| Collection            | Interface   | Ordered | Sorted | Thread safe | Duplicate | Nullable       |
|-----------------------|-------------|---------|--------|-------------|-----------|----------------|
| ArrayList             | List        | Y       | N      | N           | Y         | Y              |
| Vector                | List        | Y       | N      | Y           | Y         | Y              |
| LinkedList            | List, Deque | Y       | N      | N           | Y         | Y              |
| CopyOnWriteArrayList  | List        | Y       | N      | Y           | Y         | Y              |
| HashSet               | Set         | N       | N      | N           | N         | One null       |
| LinkedHashSet         | Set         | Y       | N      | N           | N         | One null       |
| TreeSet               | Set         | Y       | Y      | N           | N         | N              |
| CopyOnWriteArraySet   | Set         | Y       | N      | Y           | N         | One null       |
| ConcurrentSkipListSet | Set         | Y       | Y      | Y           | N         | N              |
| HashMap               | Map         | N       | N      | N           | N (key)   | One null (key) |
| HashTable             | Map         | N       | N      | Y           | N (key)   | N (key)        |
| LinkedHashMap         | Map         | Y       | N      | N           | N (key)   | One null (key) |
| TreeMap               | Map         | Y       | Y      | N           | N (key)   | N (key)        |
| ConcurrentHashMap     | Map         | N       | N      | Y           | N (key)   | N              |
| ConcurrentSkipListMap | Map         | Y       | Y      | Y           | N (key)   | N              |
| ArrayDeque            | Deque       | Y       | N      | N           | Y         | N              |
| PriorityQueue         | Queue       | Y       | N      | N           | Y         | N              |
| ConcurrentLinkedQueue | Queue       | Y       | N      | Y           | Y         | N              |
| ConcurrentLinkedDeque | Deque       | Y       | N      | Y           | Y         | N              |
| ArrayBlockingQueue    | Queue       | Y       | N      | Y           | Y         | N              |
| LinkedBlockingDeque   | Deque       | Y       | N      | Y           | Y         | N              |
| PriorityBlockingQueue | Queue       | Y       | N      | Y           | Y         | N              |

LEGENDA:  
- Interface: Interfejs, który implementuje dany typ kolekcji.
- Ordered: Czy elementy są przechowywane w kolejności w jakiej zostały dodane.
- Sorted: Czy elementy są przechowywane w uporządkowany sposób, zgodnie z ich naturalnym porządkiem.
- Thread safe: Czy kolekcja jest bezpieczna do użycia w wielowątkowym środowisku.
- Duplicate: Czy kolekcja może zawierać duplikaty elementów.
- Nullable: Czy kolekcja może zawierać wartości null.  

Wartości:
- Y: Tak
- N: Nie
- One null: Może zawierać tylko jeden null \(w przypadku HashSet, HashMap itp.\).
- One null \(key\): Może zawierać tylko jeden null jako klucz.  





