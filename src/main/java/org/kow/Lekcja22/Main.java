//22. Kurs Java dla początkujących - Kolekcje: Kolejka, Stos
//https://www.youtube.com/watch?v=Md7NCIMzPyI&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=23

package org.kow.Lekcja22;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        
        //Kolejka (od pierszego do ostatniego)
        Queue<String> kolejka = new ArrayDeque<>();
        kolejka.add("Pierwszy V1");
        kolejka.add("Drugi V1");
        kolejka.add("Pierwszy");
        kolejka.add("Drugi");
        kolejka.add("Trzecie");
        kolejka.add("Czwarty");
        
        System.out.println(kolejka.size());
        
        System.out.println("-----");
        
        //Pobiera ale nie usuwa, pierwszy element kolejki, rzuca wyjątek jak kolejka jest pusta!
        System.out.println(kolejka.element());
        //Pobiera ale nie usuwa, pierwszy element kolejki, nie rzuca wyjątkiem gdy kolejka jest pusta!
        System.out.println(kolejka.peek());
        
        System.out.println("-----");
        
        //Usuwa pierwszy element kolejki, rzuca wyjątek jak kolejka jest pusta!
        System.out.println(kolejka.remove());
        //Usuwa pierwszy element kolejki, nie rzuca wyjątkiem gdy kolejka jest pusta! (zwraca 'null')
        System.out.println(kolejka.poll());
        
        System.out.println("-----");
        
        //kolejka.clear();
        
        for (String el : kolejka) {
            System.out.println(el);
        }
        
        System.out.println("===++++++++++===");
        
        //Stos (od ostatniego do pierwszego)
        Stack<String> stos = new Stack<>();
        stos.push("Pierwszy");
        stos.push("Drugi");
        stos.push("Trzeci");
        stos.push("Czwarty");
        stos.push("Piąty");
        stos.push("Szósty");
        
        System.out.println(stos.size());
        
        System.out.println("-----");
        
        //Usuwa element ostatni
        System.out.println(stos.pop());
        //Dodaje do ostatniego elementu
        System.out.println(stos.push("Jacek Podgórni też tu jest"));
        
        System.out.println("-----");
        
        System.out.println(stos.size());
        
        System.out.println("-----");
        
        for (String el : stos) {
            System.out.println(el);
        }
        
    }
}
