//28. Kurs Java dla początkujących - Typ Enum
//https://www.youtube.com/watch?v=2hRZD_freMY&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=29

package org.kow.Lekcja28;

public class Main {

    public static void main(String[] args) {

        Samochod bmw = new Samochod("BMW", Kolory.NIEBIESKI);
        System.out.println(bmw.kolor);

        switch (bmw.kolor) {
            case BIAŁY -> System.out.println("wybrałeś kolor biały");
            case CZARNY -> System.out.println("wybrałeś kolor czarny");
            case ZIELONY -> System.out.println("wybrałeś kolor zielony");
            case CZERWONY -> System.out.println("wybrałeś kolor czerwony");
            case NIEBIESKI -> System.out.println("wybrałeś kolor niebieski");
        }

        switch (bmw.kolor) {
            case BIAŁY:
                //Polecenie
                break;
            case CZARNY:
                //Polecenie
                break;
            case ZIELONY:
                //Polecenie
                break;
            case CZERWONY:
                //Polecenie
                break;
            case NIEBIESKI:
                //Polecenie
                break;
        }

    }
}
