<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#28 - Typ Enum](https://www.youtube.com/watch?v=2hRZD_freMY&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=29)

##### Klasa `Samochod`

```java
public class Samochod {
    String marka;
    //String kolor;
    Kolory kolor;
    
    public Samochod(String marka, Kolory kolor) {
        this.marka = marka;
        this.kolor = kolor;
    }
}
```

<br>

##### Enum `Kolory`

```java
public enum Kolory {
    BIAŁY,
    CZERWONY,
    ZIELONY,
    CZARNY,
    NIEBIESKI
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        
        Samochod bmw = new Samochod("BMW", Kolory.NIEBIESKI);
        System.out.println(bmw.kolor);
        
        switch (bmw.kolor) {
            case BIAŁY -> System.out.println("wybrałeś kolor biały");
            case CZARNY -> System.out.println("wybrałeś kolor czarny");
            case ZIELONY -> System.out.println("wybrałeś kolor zielony");
            case CZERWONY -> System.out.println("wybrałeś kolor czerwony");
            case NIEBIESKI -> System.out.println("wybrałeś kolor niebieski");
        }
        
        switch (bmw.kolor) {
            case BIAŁY:
                //Polecenie
                break;
            case CZARNY:
                //Polecenie
                break;
            case ZIELONY:
                //Polecenie
                break;
            case CZERWONY:
                //Polecenie
                break;
            case NIEBIESKI:
                //Polecenie
                break;
                //DEFAULT - jest niepotrzebny
        }
    }
}
```  

> **NIEBIESKI**  
> **wybrałeś kolor niebieski**  

<br>

