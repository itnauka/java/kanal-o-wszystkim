<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#11 - Funkcje / Metody](https://www.youtube.com/watch?v=1R3zkiuxw9g&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=12)

```java
public class Main {
    public static void main(String[] args) {//sygnatura metody

        wyswietlVoid();//wywołanie metody 'void'
    }
    
    static void wyswietlVoid() {//sygnatura metody
        System.out.println("Hello World!");
    }
}
```  

> **Hello World!**  

<br />  

```java
public class Main {
    public static void main(String[] args) {//sygnatura metody
        
        wyswietlString();//zwraca string i nic nie robi (nie wyświetla)
    }

    static String wyswietlString() {//typ zwracany 'String'
        return "Hello Jacek!";
    }  
```  

> //nic nie zostanie wyświetlone

<br />  

```java
public class Main {
    public static void main(String[] args) {

        String h = wyswietlString();
        System.out.println(h);
        System.out.println(wyswietlString());//funkcja 'wyswietlString() jest argumentem funkji 'println()'
    }

    static String wyswietlString() {
        return "Hello Jacek!";
    }
```  

> **Hello Jacek!**  
> **Hello Jacek!**  

<br />  

```java
public class Main {
    public static void main(String[] args) {

        int dodaj = dodaj();
        System.out.println(dodaj);

        System.out.println(dodaj(7));
    }
    
    static int dodaj() {
        return 1 + 9;
    }
    static int dodaj(int liczbaX) {
        return ++liczbaX;//preinkrementacja i predekrementacja konieczna!!!
    }
    
    //Przeciążanie z innym typem ale bez zmiany parametrów to `błąd`
    //    static double dodaj() {
    //        return 1;
    //    }
}
```  

> **10**  
> **8**  

<br />

```java
public class Main {
    public static void main(String[] args) {

        System.out.println(dodaj());
    }
    static int dodaj() {
        return 0;
    }
    static int dodaj(int liczbaX) {
        return ++liczbaX;
    }    
    static int dodaj(int liczbaX, int liczbaY) {
        return ++liczbaX;
    }
}
```  

> **0**  

<br />

```java
public class Main {
    public static void main(String[] args) {

        System.out.println(dodaj());
        System.out.println(dodaj(5));
        System.out.println(dodaj(5, 6));
    }
    static int dodaj() {
        return 0;
    }
    static int dodaj(int liczbaX) {
        return ++liczbaX;
    }    
    static int dodaj(int liczbaX, int liczbaY) {
        return liczbaX + liczbaY;
    }
}
```  

> **0**  
> **6**  
> **11**  





