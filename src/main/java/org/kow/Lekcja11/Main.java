//11. Kurs Java dla początkujących - Funkcje / Metody
//https://www.youtube.com/watch?v=1R3zkiuxw9g&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=12

package org.kow.Lekcja11;

public class Main {
    public static void main(String[] args) {

        wyswietlVoid();

        System.out.println("------");

        String h = wyswietlString();
        System.out.println(h);

        System.out.println("------");

        int dodaj = dodaj();
        System.out.println(dodaj);

        System.out.println(dodaj(7));

        System.out.println("------");
        
        
    }

    static void wyswietlVoid() {
        System.out.println("Hello World!");
    }

    static String wyswietlString() {
        return "Hello Jacek!";
    }

    static int dodaj() {
        return 1 + 9;
    }

    //Przeciążanie z innym typem ale bez parametrów to błąd
    //    static double dodaj() {
    //        return 1;
    //    }

        static int dodaj(int liczba) {
        return ++liczba;//preinkrementacja i predekrementacja konieczna!!!
    }
    
    
}


