package org.kow.Lekcja31;

import java.util.Objects;

public class Animal {
    String name;
    int age = 5;

    @Override//Przesłanianie
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
//        return Objects.equals(name, animal.name);
        return age == animal.age && Objects.equals(name, animal.name);
    }

    @Override//Przesłanianie
    public int hashCode() {
        return Objects.hash(name, age);
    }
}

//a1.equals(a2);