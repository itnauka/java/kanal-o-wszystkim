//31. Kurs Java dla początkujących - Equals vs ==
//https://www.youtube.com/watch?v=N6iBId1EXbg&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=32

package org.kow.Lekcja31;

public class Main {

    public static void main(String[] args) {

        Animal a1 = new Animal();
        Animal a2 = new Animal();

        a1.name = "Tom";
        a2.name = "Tom";

        System.out.println("Czy 5 = 5 : " + (5 == 5));
        System.out.println("Czy A = A : " + ("A" == "A"));
        System.out.println("Czy a1 = a1 : " + (a1 == a1));//true
        System.out.println("Czy a1 = a2 : " + (a1 == a2));//false

        //a1 = a2;
        //System.out.println("Czy a1 = a2 : " + (a1 == a2));//true

        //Nadpisujemy/przesłaniamy metodę equals w klasie Animal
        System.out.println("Czy obiekty a1 = a2 : " + (a1.equals(a2)));//true

        a2.age = 2;
        System.out.println("Czy obiekty a1 = a2 : " + (a1.equals(a2)));//false
    }
}
