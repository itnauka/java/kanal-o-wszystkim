<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#31 - Equals vs ==](https://www.youtube.com/watch?v=N6iBId1EXbg&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=33)

##### Klasa `Animal`

```java
public class Animal {
    String name;
    int age = 5;
    
    @Override//Przesłanianie
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return age == animal.age && Objects.equals(name, animal.name);
    }
    
    @Override//Przesłanianie
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    
    public static void main(String[] args) {
        
        Animal a1 = new Animal();
        Animal a2 = new Animal();
        
        a1.name = "Tom";
        a2.name = "Tom";
        
        System.out.println("Czy 5 = 5 : " + (5 == 5));
        System.out.println("Czy A = A : " + ("A" == "A"));
        System.out.println("Czy a1 = a1 : " + (a1 == a1));//true
        System.out.println("Czy a1 = a2 : " + (a1 == a2));//false
    }
}
```  

> **Czy 5 = 5 : true**  
> **Czy A = A : true**  
> **Czy a1 = a1 : true**  
> **Czy a1 = a2 : false**  

<br>

##### Klasa `Main`

```java
public class Main {
    
    public static void main(String[] args) {
        
        Animal a1 = new Animal();
        Animal a2 = new Animal();
        
        a1.name = "Tom";
        a2.name = "Tom";
        
        a1 = a2;//przypisanie referencji
        System.out.println("Czy a1 = a2 : " + (a1 == a2));//true
    }
}
```  

> **Czy a1 = a2 : true**  

<br>

##### Klasa `Main`

```java
public class Main {
    
    public static void main(String[] args) {
        
        Animal a1 = new Animal();
        Animal a2 = new Animal();
        
        a1.name = "Tom";
        a2.name = "Tom";
        
        //Nadpisujemy/przesłaniamy metodę equals w klasie Animal
        System.out.println("Czy obiekty a1 = a2 : " + (a1.equals(a2)));//true
        
        a2.age = 2;
        System.out.println("Czy obiekty a1 = a2 : " + (a1.equals(a2)));//false
    }
}
```  

> **Czy obiekty a1 = a2 : true**  
> **Czy obiekty a1 = a2 : false**  

<br>



