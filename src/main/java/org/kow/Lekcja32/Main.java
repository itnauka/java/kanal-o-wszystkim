//32. Kurs Java dla początkujących - StringBuilder - optymalizacja
//https://www.youtube.com/watch?v=OsnZnKUfG44&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=33

package org.kow.Lekcja32;

public class Main {

    public static void main(String[] args) {

        int zmienna = 10_000;

        //STRING
        String string = "";
        long start = System.currentTimeMillis();

        for (int i = 0; i < zmienna; i++) {
            string += "a";
        }
        System.out.println("String: \n" + string);

        System.out.println("Czas: " + (System.currentTimeMillis() - start));

        System.out.println("------");

        //STRINGBUILDER
        StringBuilder stringBuilder = new StringBuilder();
        start = System.currentTimeMillis();

        for (int i = 0; i < zmienna; i++) {
            stringBuilder.append("a");
            //Szybsze od konkatynacji:
//            System.out.format("%s \n", "a");
        }
        System.out.println("StringBuilder: \n" + stringBuilder.toString());

        System.out.println("Czas: " + (System.currentTimeMillis() - start));

        System.out.println("------");

        //STRINGBUFFER
        StringBuffer stringBuffer = new StringBuffer();
        start = System.currentTimeMillis();

        for (int i = 0; i < zmienna; i++) {
            stringBuffer.append("a");
        }
        System.out.println("StringBuffer: \n" + stringBuffer.toString());

        System.out.println("Czas: " + (System.currentTimeMillis() - start));
    }
}
