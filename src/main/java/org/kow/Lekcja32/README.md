<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#32 - StringBuilder - optymalizacja](https://www.youtube.com/watch?v=OsnZnKUfG44&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=33)

##### `STRING`

```java
public class Main {
    public static void main(String[] args) {
        
        int zmienna = 10_000;
        
        //STRING
        String string = "";
        long start = System.currentTimeMillis();
        
        for (int i = 0; i < zmienna; i++) {
            string += "a ";
        }
        System.out.println("String: \n" + string);
        System.out.println("Czas: " + (System.currentTimeMillis() - start));
    }
}
```  

> **String: **  
> **aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa...**  
> **Czas: 46**  

<br>

##### `STRINGBUILDER`

```java
public class Main {
    public static void main(String[] args) {
        
        int zmienna = 10_000;
        
        //STRINGBUILDER
        StringBuilder stringBuilder = new StringBuilder();
        long start = System.currentTimeMillis();
        
        for (int i = 0; i < zmienna; i++) {
            stringBuilder.append("a");
//            System.out.format("%s \n", "a");
        }
        System.out.println("StringBuilder: \n" + stringBuilder.toString());
        System.out.println("Czas: " + (System.currentTimeMillis() - start));
    }
}
```  

> **StringBuilder:**  
> **aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa...**  
> **Czas: 3**  

<br>

##### `STRINGBUFFER`

```java
public class Main {
    public static void main(String[] args) {
        
        int zmienna = 10_000;
        
        //STRINGBUFFER
        StringBuffer stringBuffer = new StringBuffer();
        long start = System.currentTimeMillis();
        
        for (int i = 0; i < zmienna; i++) {
            stringBuffer.append("a");
        }
        System.out.println("StringBuffer: \n" + stringBuffer.toString());
        System.out.println("Czas: " + (System.currentTimeMillis() - start));
    }
}
```  

> **StringBuffer:**  
> **aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa...**  
> **Czas: 4**  

<br>




