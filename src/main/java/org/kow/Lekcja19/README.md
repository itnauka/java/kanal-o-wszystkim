<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#19 - Kolekcje: Lista](https://www.youtube.com/watch?v=042NlOSldnk&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=20)

##### Klasa `Animal`

```java
public class Animal {
    public String imie = "Filemon";
    
    Animal(String imie) {
        this.imie = imie;
    }
}
```  

<br>

##### Klasa `Main`

###### Lista `zwykła`  

```java
public class Main {
    public static void main(String[] args) {
        
        Animal kot1 = new Animal("Rudy");
        Animal kot2 = new Animal("Bury");
        Animal kot3 = new Animal("Biały");
        Animal kot4 = new Animal("Ciapek");
        
        ArrayList koty = new ArrayList<>();
        
        koty.add(kot1);
        koty.add(kot2);
        koty.add(kot3);
        koty.add(kot4);
        
        System.out.println("-----");
        
        for (Object kot : koty) {
            System.out.print(((Animal) kot).imie + ", \n");
        }
        
        System.out.println("-----");
        
        System.out.println(((Animal) koty.get(2)).imie);//istone nawiasy '()'
        System.out.println(koty.size());//4
        
        System.out.println("-----");
        
        System.out.println(koty.contains(kot1));// (czy istnieje)
        System.out.println(koty.contains(3));//false
        System.out.println(koty.remove(kot2));//true
        
        koty.remove(kot2);
        for (Object kot : koty) {
            System.out.print(((Animal) kot).imie + ", ");
        }
        
        koty.remove(0);
        for (Object kot : koty) {
            System.out.print(((Animal) kot).imie + ", ");
        }
        
        koty.clear();
        for (Object kot : koty) {
            System.out.print(((Animal) kot).imie + ", ");
        }
    }
}
```  

> **-----**  
> **Rudy,**  
> **Bury,**  
> **Biały,**  
> **Ciapek,**  
> **-----**  
> **Biały**  
> **4**  
> **-----**  
> **true**  
> **false**  
> **true**  
> **-----**  
> **Rudy, Biały, Ciapek,**  
> **Biały, Ciapek,**  
> **-----**  

<br>

###### Lista `generyczna`

```java
public class Main {
    public static void main(String[] args) {
        
        Animal kot1 = new Animal("Rudy");
        Animal kot2 = new Animal("Bury");
        Animal kot3 = new Animal("Biały");
        Animal kot4 = new Animal("Ciapek");
        
        ArrayList<Animal> lista = new ArrayList<>();
        lista.add(kot1);
        lista.add(kot2);
        lista.add(kot3);
        lista.add(kot4);
        
        System.out.println(lista.get(2).imie);
        
        for (Animal element : lista) {
            System.out.print(element.imie + ", ");
        }
        
        System.out.println();
        System.out.println("-----");
        
        System.out.println(lista.size());
        
        System.out.println(lista.contains(kot1));
        System.out.println(lista.contains(3));
        
        System.out.println(lista.remove(kot2));
        lista.remove(kot2);
        for (Animal element : lista) {
            System.out.print(element.imie + ", ");
        }
        
        lista.remove(0);
        for (Animal element : lista) {
            System.out.print(element.imie + ", ");
        }
        
        lista.clear();
        for (Animal element : lista) {
            System.out.print(element.imie + ", ");
        }
    }
}
```  

> **Biały**  
> **Rudy, Bury, Biały, Ciapek,**  
> **-----**  
> **4**  
> **true**  
> **false**  
> **true**  
> > **-----**  
> **Rudy, Biały, Ciapek,**
> **Biały, Ciapek,**  
> **-----**  

<br>

### Java Collections

| Collection            | Interface   | Ordered | Sorted | Thread safe | Duplicate | Nullable       |
|-----------------------|-------------|---------|--------|-------------|-----------|----------------|
| ArrayList             | List        | Y       | N      | N           | Y         | Y              |
| Vector                | List        | Y       | N      | Y           | Y         | Y              |
| LinkedList            | List, Deque | Y       | N      | N           | Y         | Y              |
| CopyOnWriteArrayList  | List        | Y       | N      | Y           | Y         | Y              |
| HashSet               | Set         | N       | N      | N           | N         | One null       |
| LinkedHashSet         | Set         | Y       | N      | N           | N         | One null       |
| TreeSet               | Set         | Y       | Y      | N           | N         | N              |
| CopyOnWriteArraySet   | Set         | Y       | N      | Y           | N         | One null       |
| ConcurrentSkipListSet | Set         | Y       | Y      | Y           | N         | N              |
| HashMap               | Map         | N       | N      | N           | N (key)   | One null (key) |
| HashTable             | Map         | N       | N      | Y           | N (key)   | N (key)        |
| LinkedHashMap         | Map         | Y       | N      | N           | N (key)   | One null (key) |
| TreeMap               | Map         | Y       | Y      | N           | N (key)   | N (key)        |
| ConcurrentHashMap     | Map         | N       | N      | Y           | N (key)   | N              |
| ConcurrentSkipListMap | Map         | Y       | Y      | Y           | N (key)   | N              |
| ArrayDeque            | Deque       | Y       | N      | N           | Y         | N              |
| PriorityQueue         | Queue       | Y       | N      | N           | Y         | N              |
| ConcurrentLinkedQueue | Queue       | Y       | N      | Y           | Y         | N              |
| ConcurrentLinkedDeque | Deque       | Y       | N      | Y           | Y         | N              |
| ArrayBlockingQueue    | Queue       | Y       | N      | Y           | Y         | N              |
| LinkedBlockingDeque   | Deque       | Y       | N      | Y           | Y         | N              |
| PriorityBlockingQueue | Queue       | Y       | N      | Y           | Y         | N              |

LEGENDA:
- Interface: Interfejs, który implementuje dany typ kolekcji.
- Ordered: Czy elementy są przechowywane w kolejności w jakiej zostały dodane.
- Sorted: Czy elementy są przechowywane w uporządkowany sposób, zgodnie z ich naturalnym porządkiem.
- Thread safe: Czy kolekcja jest bezpieczna do użycia w wielowątkowym środowisku.
- Duplicate: Czy kolekcja może zawierać duplikaty elementów.
- Nullable: Czy kolekcja może zawierać wartości null.

Wartości:
- Y: Tak
- N: Nie
- One null: Może zawierać tylko jeden null \(w przypadku HashSet, HashMap itp.\).
- One null \(key\): Może zawierać tylko jeden null jako klucz.  




