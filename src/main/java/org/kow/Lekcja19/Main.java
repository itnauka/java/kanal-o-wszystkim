//19. Kurs Java dla początkujących - Kolekcje: Lista
//https://www.youtube.com/watch?v=042NlOSldnk&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=20

package org.kow.Lekcja19;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Animal kot1 = new Animal("Rudy");
        Animal kot2 = new Animal("Bury");
        Animal kot3 = new Animal("Biały");
        Animal kot4 = new Animal("Ciapek");

        ArrayList koty = new ArrayList<>();

        koty.add(kot1);
        koty.add(kot2);
        koty.add(kot3);
        koty.add(kot4);

        System.out.println("-----");

        for (Object kot : koty) {
            System.out.print(((Animal) kot).imie + ", \n");
        }

        System.out.println("-----");

        System.out.println(((Animal) koty.get(2)).imie);

        System.out.println(koty.size());

        System.out.println(koty.contains(kot1));
        System.out.println(koty.contains(3));

        System.out.println(koty.remove(kot2));
        koty.remove(kot2);
        for (Object kot : koty) {
            System.out.print(((Animal) kot).imie + ", ");
        }

        System.out.println();
        System.out.println("-----");

        koty.remove(0);
        for (Object kot : koty) {
            System.out.print(((Animal) kot).imie + ", ");
        }

        System.out.println();
        System.out.println("-----");

        koty.clear();
        for (Object kot : koty) {
            System.out.print(((Animal) kot).imie + ", ");
        }

        System.out.println();
        System.out.println("========================");


        ArrayList<Animal> lista = new ArrayList<>();
        lista.add(kot1);
        lista.add(kot2);
        lista.add(kot3);
        lista.add(kot4);

        System.out.println(lista.get(2).imie);

        for (Animal kot : lista) {
            System.out.print(kot.imie + ", ");
        }
        System.out.println();
        System.out.println("-----");

        System.out.println(lista.size());

        System.out.println(lista.contains(kot1));
        System.out.println(lista.contains(3));

        System.out.println(lista.remove(kot2));
        lista.remove(kot2);
        for (Animal kot : lista) {
            System.out.print(kot.imie + ", ");
        }

        System.out.println();
        System.out.println("-----");

        lista.remove(0);
        for (Animal kot : lista) {
            System.out.print(kot.imie + ", ");
        }

        System.out.println();
        System.out.println("-----");

        lista.clear();
        for (Animal kot : lista) {
            System.out.print(kot.imie + ", ");
        }
    }

}
