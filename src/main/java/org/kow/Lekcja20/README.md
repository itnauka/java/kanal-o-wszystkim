<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#20 - Kolekcje: Map, Set](https://www.youtube.com/watch?v=Qae77e0_6tQ&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=21)  

#### LinkedList  

```java
public class Main {
    public static void main(String[] args) {
        
        //LinkedList
        LinkedList<String> listy = new LinkedList<>();
        
        //Z duplikatami 8 elementów
        listy.add("Poznań");
        listy.add("Kraków");
        listy.add("Wrocław");
        listy.add("Wrocław");
        listy.add("Wrocław");
        listy.add("Wrocław");
        listy.add("Nowy Sącz");
        listy.add("Limanowa");
        
        for (String miasto : listy) {
            System.out.println(miasto);
        }
        
        System.out.println("----");
        
        listy.clear();
        System.out.println("Czy jest pusta: " + listy.isEmpty());
        for (String miasto : listy) {
            System.out.println(miasto);
        }
    }
}
```  

> **Poznań**  
> **Kraków**  
> **Wrocław**  
> **Wrocław**  
> **Wrocław**  
> **Wrocław**  
> **Nowy Sącz**  
> **Limanowa**  
> **----**  
> **Czy jest pusta: true**  

<br>

#### HashMap  

```java
public class Main {
    public static void main(String[] args) {
        
        //HashMap
        HashMap<Integer, String> mapy = new HashMap<>();
        mapy.put(1, "Poniedziałek");
        mapy.put(2, "Wtorek");
        mapy.put(3, "Środa");
        mapy.put(4, "Czwartek");
        mapy.put(5, "Piątek");
        mapy.put(5, "Piątunio");//Nadpisanie
        mapy.put(6, "Sobota");
        mapy.put(7, "Niedziela");
        mapy.put(7, "Niedziela");//Nadpisanie
        
        for (String value : mapy.values()) {
            System.out.println(value);
        }
        
        System.out.println("----");
        
        System.out.println(mapy.get(4));
    }
}
```  

> **Poniedziałek**  
> **Wtorek**  
> **Środa**  
> **Czwartek**  
> **Piątunio**  
> **Sobota**  
> **Niedziela**  
> **----**  
> **Czwartek**  

<br>

#### HashSet  

```java
public class Main {
    public static void main(String[] args) {
        
        //HashSet - wyświetlana random'owo, bez duplikatów
        HashSet<String> sety = new HashSet<>();//LinkedHashSet w kolejności dodawania
        
        //Nie zachowuje kolejności wprowadzanych danych
        //Z duplikatami 8 elementów
        sety.add("Poznań");
        sety.add("Kraków");
        sety.add("Wrocław");
        sety.add("Wrocław");
        sety.add("Wrocław");
        sety.add("Wrocław");
        sety.add("Nowy Sącz");
        sety.add("Limanowa");
        
        System.out.println("Rozmiar seta: " + sety.size());
        
        for (String set : sety) {
            System.out.println(set);
        }
    }
}
```  

> **Rozmiar seta: 5**  
> **Kraków**  
> **Wrocław**  
> **Nowy Sącz**  
> **Limanowa**  
> **Poznań**

<br>

#### LinkedHashSet  

```java
public class Main {
    public static void main(String[] args) {
        
        //HashSet - wyświetlana random'owo, bez duplikatów
        LinkedHashSet<String> sety = new LinkedHashSet<>();//LinkedHashSet w kolejności dodawania
        
        //Zachowuje kolejność wprowadzanych danych
        sety.add("Poznań");
        sety.add("Kraków");
        sety.add("Wrocław");
        sety.add("Wrocław");
        sety.add("Wrocław");
        sety.add("Wrocław");
        sety.add("Nowy Sącz");
        sety.add("Limanowa");
        
        System.out.println("Rozmiar seta: " + sety.size());
        
        for (String set : sety) {
            System.out.println(set);
        }
    }
}
```  

> **Rozmiar seta: 5** 
> **Poznań**  
> **Kraków**  
> **Wrocław**  
> **Nowy Sącz**  
> **Limanowa**  


<br>

### Java Collections  

| Collection            | Interface   | Ordered | Sorted | Thread safe | Duplicate | Nullable       |
|-----------------------|-------------|---------|--------|-------------|-----------|----------------|
| ArrayList             | List        | Y       | N      | N           | Y         | Y              |
| Vector                | List        | Y       | N      | Y           | Y         | Y              |
| LinkedList            | List, Deque | Y       | N      | N           | Y         | Y              |
| CopyOnWriteArrayList  | List        | Y       | N      | Y           | Y         | Y              |
| HashSet               | Set         | N       | N      | N           | N         | One null       |
| LinkedHashSet         | Set         | Y       | N      | N           | N         | One null       |
| TreeSet               | Set         | Y       | Y      | N           | N         | N              |
| CopyOnWriteArraySet   | Set         | Y       | N      | Y           | N         | One null       |
| ConcurrentSkipListSet | Set         | Y       | Y      | Y           | N         | N              |
| HashMap               | Map         | N       | N      | N           | N (key)   | One null (key) |
| HashTable             | Map         | N       | N      | Y           | N (key)   | N (key)        |
| LinkedHashMap         | Map         | Y       | N      | N           | N (key)   | One null (key) |
| TreeMap               | Map         | Y       | Y      | N           | N (key)   | N (key)        |
| ConcurrentHashMap     | Map         | N       | N      | Y           | N (key)   | N              |
| ConcurrentSkipListMap | Map         | Y       | Y      | Y           | N (key)   | N              |
| ArrayDeque            | Deque       | Y       | N      | N           | Y         | N              |
| PriorityQueue         | Queue       | Y       | N      | N           | Y         | N              |
| ConcurrentLinkedQueue | Queue       | Y       | N      | Y           | Y         | N              |
| ConcurrentLinkedDeque | Deque       | Y       | N      | Y           | Y         | N              |
| ArrayBlockingQueue    | Queue       | Y       | N      | Y           | Y         | N              |
| LinkedBlockingDeque   | Deque       | Y       | N      | Y           | Y         | N              |
| PriorityBlockingQueue | Queue       | Y       | N      | Y           | Y         | N              |

LEGENDA:  
- Interface: Interfejs, który implementuje dany typ kolekcji.
- Ordered: Czy elementy są przechowywane w kolejności w jakiej zostały dodane.
- Sorted: Czy elementy są przechowywane w uporządkowany sposób, zgodnie z ich naturalnym porządkiem.
- Thread safe: Czy kolekcja jest bezpieczna do użycia w wielowątkowym środowisku.
- Duplicate: Czy kolekcja może zawierać duplikaty elementów.
- Nullable: Czy kolekcja może zawierać wartości null.  

Wartości:
- Y: Tak
- N: Nie
- One null: Może zawierać tylko jeden null \(w przypadku HashSet, HashMap itp.\).
- One null \(key\): Może zawierać tylko jeden null jako klucz.  





