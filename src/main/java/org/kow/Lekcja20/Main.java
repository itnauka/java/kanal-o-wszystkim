//20. Kurs Java dla początkujących - Kolekcje: Map, Set
//https://www.youtube.com/watch?v=Qae77e0_6tQ&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=21

package org.kow.Lekcja20;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {

        //LinkedList
        LinkedList<String> listy = new LinkedList<>();

        //Z duplikatami 8 elementów
        listy.add("Poznań");
        listy.add("Kraków");
        listy.add("Wrocław");
        listy.add("Wrocław");
        listy.add("Wrocław");
        listy.add("Wrocław");
        listy.add("Nowy Sącz");
        listy.add("Limanowa");

        for (String miasto : listy) {
            System.out.println(miasto);
        }

        System.out.println("----");

        listy.clear();
        System.out.println("Czy jest pusta: " + listy.isEmpty());
        for (String miasto : listy) {
            System.out.println(miasto);
        }

        System.out.println("============");

        //HashMap
        HashMap<Integer, String> mapy = new HashMap<>();
        mapy.put(1, "Poniedziałek");
        mapy.put(2, "Wtorek");
        mapy.put(3, "Środa");
        mapy.put(4, "Czwartek");
        mapy.put(5, "Piątek");
        mapy.put(5, "Piątunio");//Nadpisanie
        mapy.put(6, "Sobota");
        mapy.put(7, "Niedziela");
        mapy.put(7, "Niedziela");//Nadpisanie

        for (String value : mapy.values()) {
            System.out.println(value);
        }

        System.out.println("----");

        System.out.println(mapy.get(4));

        System.out.println("============");

        //HashSet - wyświetlana random'owo, bez duplikatów
        HashSet<String> sety = new HashSet<>();//LinkedHashSet w kolejności dodawania

        //Z duplikatami 8 elementów
        sety.add("Poznań");
        sety.add("Kraków");
        sety.add("Wrocław");
        sety.add("Wrocław");
        sety.add("Wrocław");
        sety.add("Wrocław");
        sety.add("Nowy Sącz");
        sety.add("Limanowa");

        System.out.println("Rozmiar seta: " + sety.size());

        for (String set : sety) {
            System.out.println(set);
        }
        
        System.out.println("===============");
        //LinkedHashSet - wyświetlana zgodnie z kolejnością, bez duplikatów
        LinkedHashSet<String> linkedSety = new LinkedHashSet<>();//LinkedHashSet w kolejności dodawania

        //Z duplikatami 8 elementów
        linkedSety.add("Poznań");
        linkedSety.add("Kraków");
        linkedSety.add("Wrocław");
        linkedSety.add("Wrocław");
        linkedSety.add("Wrocław");
        linkedSety.add("Wrocław");
        linkedSety.add("Nowy Sącz");
        linkedSety.add("Limanowa");

        System.out.println("Rozmiar seta: " + linkedSety.size());

        for (String set : linkedSety) {
            System.out.println(set);
        }


    }
}
