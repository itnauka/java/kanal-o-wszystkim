//25. Kurs Java dla początkujących - Dziedziczenie cz. 3 (Metody, abstract)
//https://www.youtube.com/watch?v=0rq1FUGMpmU&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=26

package org.kow.Lekcja25;

public class Main {
    public static void main(String[] args) {

        Cat kot = new Cat();
        kot.dajGlos();
        kot.idz();

        Animal pies = new Dog();
        pies.dajGlos();
        ((Dog)pies).idz();//Rzutowanie

        //Animal chomik = new Animal();//Błąd >> klasa abstrakcyjna
    }
}
