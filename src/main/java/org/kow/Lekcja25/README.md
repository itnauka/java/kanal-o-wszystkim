<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#25 - Dziedziczenie cz. 3 (Metody, abstract)](https://www.youtube.com/watch?v=0rq1FUGMpmU&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=26)

##### Klasa `Animal`

```java
public abstract class Animal {
    
    abstract public void dajGlos();
    abstract public void idz();
    
    public void test() {
        System.out.println("Metoda w klasie abstrakcyjnej");
    }

//    abstract public void dajGlos(){
//        System.out.println("Grrr");
//    }
}
```

<br>

##### Klasa `Dog`

```java
public class Dog extends Animal {
    @Override
    public void dajGlos() {
        System.out.println("How How");
    }
    
    @Override
    public void idz() {
        System.out.println("Pies spaceruje z Panem");
    }
}
```

<br>

##### Klasa `Cat`

```java
public class Cat extends Animal {
    
    @Override
    public void dajGlos() {
        System.out.println("Meow Meow");
    }
    
    @Override
    public void idz() {
        System.out.println("Kot spaceruje");
    }
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        
        Cat kot = new Cat();
        kot.dajGlos();
        kot.idz();
        
        Animal pies = new Dog();
        pies.dajGlos();
        ((Dog)pies).idz();//Rzutowanie żeby dostać sie do metody klasy 'Dog'
        
        //Animal chomik = new Animal();//Błąd >> klasa abstrakcyjna
    }
}
```  

> **Meow Meow**  
> **Kot spaceruje**  
> **How How**  
> **Pies spaceruje z Panem**  

<br>

