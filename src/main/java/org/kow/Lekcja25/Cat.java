package org.kow.Lekcja25;

public class Cat extends Animal {

    @Override
    public void dajGlos() {
        System.out.println("Meow Meow");
    }

    @Override
    public void idz() {
        System.out.println("Kot spaceruje");
    }
}
