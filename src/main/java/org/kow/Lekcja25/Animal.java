package org.kow.Lekcja25;

public abstract class Animal {

    abstract public void dajGlos();
    abstract public void idz();

    public void test() {
        System.out.println("Metoda w klasie abstrakcyjnej");
    }

//    abstract public void dajGlos(){
//        System.out.println("Grrr");
//    }
}