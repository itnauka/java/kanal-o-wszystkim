//30. Kurs Java dla początkujących - Wielowątkowość - Thread
//https://www.youtube.com/watch?v=RpDIMnB9yFg&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=31

package org.kow.Lekcja30;

public class Main {
    public static void main(String[] args) {

//        Obliczenia obl1 = new Obliczenia();
//        Obliczenia obl2 = new Obliczenia();
//
//        //Jednowątkowo
//        obl1.run();
//        System.out.println("-----");
//        obl2.run();
//
//        System.out.println("===++++++===");
//
//        //Wielowątkowo
//        obl1.start();
//        System.out.println("-----");
//        obl2.start();
//
//        System.out.println("===++++++===");

        Thread obl3 = new Thread(new ObliczeniaIMPL());
        Thread obl4 = new Thread(new ObliczeniaIMPL());
        obl3.start();
        obl4.start();
    }
}
