<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#34 - Rekurencja](https://www.youtube.com/watch?v=0ntw-KXxVZw&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=35)

```java
public class Main {
    public static void main(String[] args) {
        final int SILNIA = 10;
        
        long start = System.nanoTime();
        System.out.println(silnia(SILNIA));
        System.out.println("Czas: " + (System.nanoTime() - start));
        
        System.out.println("-------");
        
        start = System.nanoTime();
        System.out.println(silniaRek(SILNIA));
        System.out.println("Czas: " + (System.nanoTime() - start));
    }
    
    private static long silniaRek(int silnia) {
        if (silnia <= 1)
            return 1;
        else
            return silnia * silniaRek(--silnia);//Lub (silnia - 1)
    }
    private static long silnia(int silnia) {
        long wynik = 1;
        while (silnia > 1) {
            wynik *= silnia;
            silnia--;
        }
        return wynik;
    }
}
```  

> **3628800**  
> **Czas: 497490**  
> **-------**  
> **3628800**  
> **Czas: 72743**  

<br>




