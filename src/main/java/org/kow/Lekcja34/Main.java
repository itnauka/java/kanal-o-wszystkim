//34. Kurs Java dla początkujących - Rekurencja
//https://www.youtube.com/watch?v=0ntw-KXxVZw&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=35

package org.kow.Lekcja34;

public class Main {
    public static void main(String[] args) {
        final int SILNIA = 20;

        long start = System.nanoTime();
        System.out.println(silnia(SILNIA));
        System.out.println("Czas: " + (System.nanoTime() - start));

        System.out.println("-------");

        start = System.nanoTime();
        System.out.println(silniaRek(SILNIA));
        System.out.println("Czas: " + (System.nanoTime() - start));

    }

    private static long silniaRek(int silnia) {
        if (silnia <= 1) return 1;
        else return silnia * silniaRek(--silnia);//Lub (silnia - 1)
    }
    private static long silnia(int silnia) {
        long wynik = 1;
        while (silnia > 1) {
            wynik *= silnia;
            silnia--;
        }
        return wynik;
    }
}
