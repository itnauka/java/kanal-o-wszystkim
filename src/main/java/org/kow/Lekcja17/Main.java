//17. Kurs Java dla początkujących - Gettery i Settery hermetyzacja danych
//https://www.youtube.com/watch?v=Xxy5oz3EdmA&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=18

package org.kow.Lekcja17;

import org.kow.Lekcja16.Liczba;

public class Main {
    public static void main(String[] args) {
        Uczen u1 = new Uczen();
        //u1.nazwisko = "Nowak";
        //u1.ocena = 10000;
        //System.out.println(u1.ocena);

        u1.setNazwisko("Kowalski");
        System.out.println(u1.getNazwisko());
        u1.setNazwisko("K");
        System.out.println(u1.getNazwisko());

        System.out.println("-----");

        u1.setOcena(5);
        System.out.println(u1.getOcena());
        u1.setOcena(8);
        System.out.println(u1.getOcena());

    }
}
