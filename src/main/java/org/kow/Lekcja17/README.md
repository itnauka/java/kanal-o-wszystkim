<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#17 - Gettery i Settery hermetyzacja danych](https://www.youtube.com/watch?v=Xxy5oz3EdmA&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=18)

##### Klasa `Liczba`  

```java
public class Uczen {
    private String nazwisko;
    private int ocena;
    
    public String getNazwisko() {
        return "Nazwisko: " + this.nazwisko;
    }
    public void setNazwisko(String nazwisko) {
        if (nazwisko.length() >= 2) {
            this.nazwisko = nazwisko;
        }
    }
    
    public int getOcena() {
        return this.ocena;
    }
    public void setOcena(int ocena) {
        if (ocena >= 1 && ocena <= 6) {
            this.ocena = ocena;
        }
    }
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        Uczen u1 = new Uczen();
        
        //Dostęp do pól (public) klasy
        //u1.nazwisko = "Nowak";
        //u1.ocena = 10000;
        //System.out.println(u1.ocena);
        
        u1.setNazwisko("Kowalski");
        System.out.println(u1.getNazwisko());
        u1.setNazwisko("K");//'K' < 2, warunek nie jest spełniony, zostaje poprzednie
        System.out.println(u1.getNazwisko());
        
        System.out.println();
        
        u1.setOcena(5);
        System.out.println(u1.getOcena());
        u1.setOcena(8);
        System.out.println(u1.getOcena());
    }
}
```  

> **Nazwisko: Kowalski**  
> **Nazwisko: Kowalski**  
>  
> **5**  
> **5**  

<br>





