package org.kow.Lekcja17;

public class Uczen {
    private String nazwisko;
    private int ocena;

    public String getNazwisko() {
        return "Nazwisko: " + this.nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        if (nazwisko.length() >= 2) {
            this.nazwisko = nazwisko;
        }
    }

    public int getOcena() {
        return this.ocena;
    }

    public void setOcena(int ocena) {
        if (ocena >= 1 && ocena <= 6) {
            this.ocena = ocena;
        }
    }



}
