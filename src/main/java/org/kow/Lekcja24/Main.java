//24. Kurs Java dla początkujących - Dziedziczenie cz. 2 (Konstruktory)
//https://www.youtube.com/watch?v=8mVYuzNTMQg&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=25

package org.kow.Lekcja24;

public class Main {
    public static void main(String[] args) {
        Samochod audi = new Samochod("Audi", 4, 2);
        audi.iloscKol = 4;
        audi.marka = "Audi";

        System.out.println("-----");

        SamochodSportowy subaru = new SamochodSportowy("Subaru", 4, 4);

        System.out.println("-----");

    }
}
