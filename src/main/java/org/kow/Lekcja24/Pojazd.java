package org.kow.Lekcja24;

public class Pojazd {
    String marka;
    int iloscKol;

//    Pojazd() {
//        System.out.println("Konstruktor klasy: Pojazd");
//    }

    Pojazd(String marka, int iloscKol) {
        this.marka = marka;
        this.iloscKol = iloscKol;
        System.out.println("Konstruktor klasy: Pojazd");
    }

    protected void odpal() {
        System.out.println("Pojazd odpalił.");
    }
}
