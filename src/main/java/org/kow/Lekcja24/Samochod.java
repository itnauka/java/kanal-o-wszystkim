package org.kow.Lekcja24;

public class Samochod extends Pojazd {
    boolean otwarty;
    int iloscDrzwi;

    Samochod(String marka, int iloscKol, int iloscDrzwi) {
        super(marka, iloscKol);
        this.iloscDrzwi = iloscDrzwi;
        System.out.println("Konstruktor klasy: Samochod");
    }

    void zamknijOtworz() {
        otwarty = !otwarty;
        if (otwarty) System.out.println("Otwarty");
        else System.out.println("Zamknięty");
    }
}
