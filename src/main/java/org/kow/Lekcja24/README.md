<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#24 - Dziedziczenie cz. 2 (Konstruktory)](https://www.youtube.com/watch?v=8mVYuzNTMQg&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=25)

##### Klasa `Pojazd`

```java
public class Pojazd {
    String marka;
    int iloscKol;

//    Pojazd() {
//        System.out.println("Konstruktor klasy: Pojazd");
//    }
    
    Pojazd(String marka, int iloscKol) {
        this.marka = marka;
        this.iloscKol = iloscKol;
        System.out.println("Konstruktor klasy: Pojazd");
    }
    
    protected void odpal() {
        System.out.println("Pojazd odpalił.");
    }
}
```

<br>

##### Klasa `Samochod`

```java
public class Samochod extends Pojazd {
    boolean otwarty;
    int iloscDrzwi;
    
    Samochod(String marka, int iloscKol, int iloscDrzwi) {
        super(marka, iloscKol);
        this.iloscDrzwi = iloscDrzwi;
        System.out.println("Konstruktor klasy: Samochod");
    }
    
    void zamknijOtworz() {
        otwarty = !otwarty;
        if (otwarty) System.out.println("Otwarty");
        else System.out.println("Zamknięty");
    }
}
```

<br>

##### Klasa `SamochodSportowy`

```java
public class SamochodSportowy extends Samochod {
    
    SamochodSportowy(String marka, int iloscKol, int iloscDrzwi) {
        super(marka, iloscKol, iloscDrzwi);
        System.out.println("Konstruktor klasy: Samochód Sportowy!!!");
    }
}
```

<br>

##### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        Samochod audi = new Samochod("Audi", 4, 2);
        audi.iloscKol = 4;
        audi.marka = "Audi";
        
        System.out.println("-----");
        
        SamochodSportowy subaru = new SamochodSportowy("Subaru", 4, 4);
    }
}
```  

> **Konstruktor klasy: Pojazd**  
> **Konstruktor klasy: Samochod**  
> **-----**  
> **Konstruktor klasy: Pojazd**  
> **Konstruktor klasy: Samochod**  
> **Konstruktor klasy: Samochód Sportowy!!!**  

<br>

