<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#2 - Typy danych, zmienne i stałe](https://www.youtube.com/watch?v=sp10JnpVwlc&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=3)

```java
public class Main {
    public static void main(String[] args) {

//  - Komentarz jednoliniowy
/*  - Komentarz blokowy /wielolinijkowy/
    JAVA Język SILNIE TYPOWANY

    Nazwy zmiennych:
        char zmiennaChar
        char zmienna_char
        char zmiennaChar1
*/

        // Znak
        char zmiennaChar = 'B';//litery lub liczby ASCII
        System.out.println(zmiennaChar);

        // String
        String zmiennaString = "Jacek"; //napisy
        System.out.println(zmiennaString);

        //Boolean
        boolean zmiennaBoolean = true; //lub false
        System.out.println(zmiennaBoolean);

        //Byte
        byte zmiennaByte1 = Byte.MIN_VALUE;
        byte zmiennaByte2 = Byte.MAX_VALUE;
        System.out.println("Byte: " + zmiennaByte1 + " - " + zmiennaByte2);

        //Short
        short zmiennaShort1 = Short.MIN_VALUE;
        short zmiennaShort2 = Short.MAX_VALUE;
        System.out.println("Short: " + zmiennaShort1 + " - " + zmiennaShort2);

        //Integer
        int zmiennaInt1 = Integer.MIN_VALUE;
        int zmiennaInt2 = Integer.MAX_VALUE;
        System.out.println("Integer: " + zmiennaInt1 + " - " + zmiennaInt2);

        //Long
        long zmiennaLong = 45475675345345L;
        long zmiennaLong1 = Long.MIN_VALUE;
        long zmiennaLong2 = Long.MAX_VALUE;
        System.out.println("Long: " + zmiennaLong1 + " - " + zmiennaLong2);

        //float
        float zmiennaFloat = 28.7F;
        float zmiennaFloat1 = Float.MIN_VALUE;
        float zmiennaFloat2 = Float.MAX_VALUE;
        System.out.println("Float: " + zmiennaFloat1 + " - " + zmiennaFloat2);

        //Double
        double zmiennaDouble1 = Double.MIN_VALUE;
        double zmiennaDouble2 = Double.MAX_VALUE;
        System.out.println("Double: " + zmiennaDouble1 + " - " + zmiennaDouble2);

        //Stała
//        final int liczba = 256;//Stała = final
//        liczba = 3;
    }
}
```  

> **B**  
> **Jacek**  
> **true**  
> **Byte: -128 - 127**  
> **Short: -32768 - 32767**  
> **Integer: -2147483648 - 2147483647**  
> **Long: -9223372036854775808 - 9223372036854775807**  
> **Float: 1.4E-45 - 3.4028235E38**  
> **Double: 4.9E-324 - 1.7976931348623157E308**

<br />



