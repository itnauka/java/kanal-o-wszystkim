<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#12 - Wysyłanie argumentów do funkcji](https://www.youtube.com/watch?v=gqXlGOLE8EE&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=13)

##### Typy proste  

```java
public class Main {
    public static void main(String[] args) {
        int a = 25;
        int b = a;//kopiowanie wartości
        b = 10;
        System.out.println("Wartość a = " + a);
    }
}
```  

> **Wartość a = 25**

<br>

```java
public class Main {
    public static void main(String[] args) {
        
        //typy proste
        int y = 5;
        zmienTypProsty(y);
        System.out.println("y = " + y);
        
        y = zwrocTypProsty(y);
        System.out.println("Po wyjściu z metody y = " + y);
        
        System.out.println("------");
    }
    
    public static void zmienTypProsty(int x) {
        x += 5;
        System.out.println("W metodzie: " + x);
    }
    public static int zwrocTypProsty(int x) {
        x += 5;
        System.out.println("W metodzie zwrocTypProsty, x = " + x);
        return x;
    }
```  

> **W metodzie: 10**  
> **y = 5**  
> **W metodzie zwrocTypProsty, x = 10**  
> **Po wyjściu z metody y = 10**  

<br>

##### Typy złożone

```java
public class Main {
    public static void main(String[] args) {
        int[] y = {5};
        int[] x = y;//kopiowanie referencji
        x[0] = 1024;
        System.out.println("Wartość y[0] = " + y[0]);
    }
}
```  

> **Wartość y[0] = 1024**

<br>

```java
public class Main {
    public static void main(String[] args) {
        
         //typy złożone
        int[] y = {5};
        System.out.println("index[0] = " + y[0]);
        
        zmienTypZlozony(y);
        System.out.println("index[0] = " + y[0]);
    }
    
    public static void zmienTypZlozony(int[] x) {
        x[0] += 5;
        System.out.println("W metodzie: " + x[0]);
    }
```  

> **index[0] = 5**  
> **W metodzie: 10**  
> **index[0] = 10**  

<br>

```java
public class Main {
    public static void main(String[] args) {
        
         //typy złożone
        int[] y = {5};
        System.out.println("index[0] = " + y[0]);
        
        y = zmienTypZlozony(y);
        System.out.println("index[0] = " + y[0]);
    }
    
    public static int[] zmienTypZlozony(int[] x) {
        x = new int[1];//Inna referencja
        x[0] += 20;
        System.out.println("W metodzie: " + x[0]);
        return x;
    }
```  

> **index[0] = 5**  
> **W metodzie: 20**  
> **index[0] = 20**  

<br>

##### Typy złożone niemutowalne (\'String'\)  

```java
public class Main {
    public static void main(String[] args) {
        
        String string1 = "Kowalski";
        String string2 = string1;
        string2 = "Marian";
        System.out.println(string1);
        System.out.println(string2);
        
        zmien(string1);
        System.out.println(string1.length());
        System.out.println(string1);
    }
    
    public static int[] zmienTypZlozony(int[] x) {
        x = new int[1];//Inna referencja
        x[0] += 20;
        System.out.println("W metodzie: " + x[0]);
        return x;
    }
```  

> **Podgórni**  
> **Jacek**  
> **Metoda: Podgórni Jacek**  
> **8**  
> **Podgórni**  

<br>






