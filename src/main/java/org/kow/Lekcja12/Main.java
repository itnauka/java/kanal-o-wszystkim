//12. Kurs Java dla początkujących - Wysyłanie argumentów do funkcji
//https://www.youtube.com/watch?v=gqXlGOLE8EE&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=13

package org.kow.Lekcja12;

public class Main {
    public static void main(String[] args) {

        //typy proste
        int y = 5;
        zmienTypProsty(y);
        System.out.println("y = " + y);

        y = zwrocTypProsty(y);
        System.out.println("Po wyjściu z metody y = " + y);

        System.out.println("------");

        //typy złożone
        int[] tab = {5};
        System.out.println("index[0] = " + tab[0]);

        zmienTypZlozony(tab);
        System.out.println("index[0] = " + tab[0]);
        
        System.out.println("------");
        
        int[] indexTab = {20};
        System.out.println("index[0] = " + indexTab[0]);
        
        zwrocIndexTablicy(indexTab[0]);
        System.out.println("index[0] = " + indexTab[0]);
        
        System.out.println("------");
        
        //Typ String jest 'FINAL' (niemutowalny)
        String string1 = "Podgórni";
        String string2 = string1;
        string2 = "Jacek"; //za każdym razem tworzony jest nowy obiekt (nie zmienna) na który wskazuje zmienna (w tym przypadku string2)
        System.out.println(string1);
        System.out.println(string2);
        
        zmien(string1);
        System.out.println(string1.length());
        System.out.println(string1);
    }

    public static void zmienTypProsty(int x) {
        x += 5;
        System.out.println("W metodzie: " + x);
    }
    public static int zwrocTypProsty(int x) {
        x += 5;
        System.out.println("W metodzie zwrocTypProsty, x = " + x);
        return x;
    }

    public static void zmienTypZlozony(int[] tablica) {
        tablica[0] += 5;
        System.out.println("W metodzie: " + tablica[0]);
    }
    
    public static void zwrocIndexTablicy(int x) {
        x += 101;
        System.out.println("zwrocIndexTablicy w metodzie: " + x);
    }
    
    private static void zmien(String s) {
        s += " Jacek";
        System.out.println("Metoda: " + s);
    }
}

