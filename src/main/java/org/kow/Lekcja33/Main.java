//33. Kurs Java dla początkujących - Zapis do pliku (File)
//https://www.youtube.com/watch?v=i1xNvCG8_MI&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=34

package org.kow.Lekcja33;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Formatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //Ustawienie wskaźnika na plik (ŚCEŻKĘ)
        File file = new File("/home/cyfra/Documents/PORTFOLIO/JAVA" +
                "/Kanal o Wszystkim/Kurs Java 2.0/src/main/java/org/kow/Lekcja33/plik.txt");

        //Jeżeli plik nie istnieje
        if (!file.exists()) {
            try {
                //Tworzymy plik 'plik.txt'
                file.createNewFile();
                System.out.println("Plik został utworzony");
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        if (file.canWrite()) {
            try {

                //Zapisywanie do pliku kolejnych wierszy 'true'
                FileWriter fileWriter = new FileWriter(file, true);
                //Formatowanie tekstu (wierszy)
                Formatter formatter = new Formatter(fileWriter);
                //Zczytywanie z pliku
                Scanner scannerFile = new Scanner(file);

                //Zczytywanie z konsoli od użytkownika
                Scanner scanner = new Scanner(System.in);

                System.out.print("Podaj tekst do pliku: ");
                String tekst = scanner.nextLine();
                formatter.format("%s \r\n", tekst);//Dla Microsoft Windows - 2 znaki nowej linii \r\n
                System.out.println("Zapisano w pliku!");

                formatter.close();
                fileWriter.close();

                System.out.println("\nZAWARTŚĆ PLIKU:");
                while (scannerFile.hasNextLine()) {
                    System.out.println(scannerFile.nextLine());
                }

                scanner.close();
                scannerFile.close();

            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
