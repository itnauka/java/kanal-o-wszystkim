<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących  

### [#21 - Kolekcje: operacje, sortowanie](https://www.youtube.com/watch?v=E4z0TtaEiGs&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=22)  

#### Klasa `Main`

```java
public class Main {
    public static void main(String[] args) {
        
        //Lista typu generycznego
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Poznań");
        lista.add("Kraków");
        lista.add("Wrocław");
        lista.add("Nowy Sącz");
        lista.add("Limanowa");
        lista.add("Szczecin");

        for (String miasto : lista) {
            System.out.println(miasto);
        }

        System.out.println("-------");

        Collections.sort(lista);

        for (String miasto : lista) {
            System.out.println(miasto);
        }
    }
}
```  

> **Poznań**  
> **Kraków**  
> **Wrocław**  
> **Nowy Sącz**  
> **Limanowa**  
> **Szczecin**  
> **-------**  
> **Kraków**  
> **Limanowa**  
> **Nowy Sącz**  
> **Poznań**  
> **Szczecin**  
> **Wrocław**  

<br>

```java
public class Main {
    public static void main(String[] args) {
        
        
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Poznań");
        lista.add("Kraków");
        lista.add("Wrocław");
        lista.add("Nowy Sącz");
        lista.add("Limanowa");
        lista.add("Szczecin");

        for (String miasto : lista) {
            System.out.println(miasto);
        }

        System.out.println(Collections.min(lista));
    }
}
```  

> **Kraków**

<br>

```java
public class Main {
    public static void main(String[] args) {
        
        
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Poznań");
        lista.add("Kraków");
        lista.add("Wrocław");
        lista.add("Nowy Sącz");
        lista.add("Limanowa");
        lista.add("Szczecin");

        for (String miasto : lista) {
            System.out.println(miasto);
        }
        
        System.out.println(Collections.max(lista));
    }
}
```  

> **Wrocław**

<br>

```java
public class Main {
    public static void main(String[] args) {
        
        
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Poznań");
        lista.add("Kraków");
        lista.add("Wrocław");
        lista.add("Nowy Sącz");
        lista.add("Limanowa");
        lista.add("Szczecin");

        for (String miasto : lista) {
            System.out.println(miasto);
        }
        
        Collections.reverse(lista);
        for (String miasto : lista) {
            System.out.println(miasto);
        }
    
    }
}
```  

> **Wrocław**  
> **Szczecin**  
> **Poznań**  
> **Nowy Sącz**  
> **Limanowa**  
> **Kraków**  
> 
<br>

```java
public class Main {
    public static void main(String[] args) {
        
        
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Poznań");
        lista.add("Kraków");
        lista.add("Wrocław");
        lista.add("Nowy Sącz");
        lista.add("Limanowa");
        lista.add("Szczecin");

        for (String miasto : lista) {
            System.out.println(miasto);
        }
        
        Collections.shuffle(lista);//potasuj, prztasuj
        for (String miasto : lista) {
            System.out.println(miasto);
        }
    
    }
}
```  

> **Nowy Sącz**  
> **Szczecin**  
> **Poznań**  
> **Limanowa**  
> **Wrocław**  
> **Kraków**  

<br>

#### Klasa `Animal`

```java
public class Animal implements Comparable<Animal>{
    String name;
    int wiek = 5;

    Animal(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Animal o) {
        System.out.println(this.name.compareTo(o.name));
        if (this.name.compareTo(o.name) != 0) {
            return this.name.compareTo(o.name);
        }
        return this.wiek - o.wiek;
    }
}
```

<br>

```java
public class Main {
    public static void main(String[] args) {

        ArrayList<Animal> koty = new ArrayList<>();
        Animal kot1 = new Animal("Bury");
        Animal kot2 = new Animal("Filemon");
        Animal kot3 = new Animal("Ciapek");
        Animal kot4 = new Animal("Fajtłapa");
        Animal kot5 = new Animal("Sylwester");
        Animal kot6 = new Animal("Biały");
        Animal kot7 = new Animal("Biały");
        kot4.wiek = 10;
        kot6.wiek = 15;


        koty.add(kot1);
        koty.add(kot2);
        koty.add(kot3);
        koty.add(kot4);
        koty.add(kot5);
        koty.add(kot6);
        koty.add(kot7);

        for (Animal kot : koty) {
            System.out.println(kot.name + " " + kot.wiek + " lat.");
        }

        System.out.println("-------");

        Collections.sort(koty);
        for (Animal kot : koty) {
            System.out.println(kot.name + " " + kot.wiek + " lat.");
        }
    }
}
```  

> **Bury 5 lat.** 
> **Filemon 5 lat.**  
> **Ciapek 5 lat.**  
> **Fajtłapa 10 lat.**  
> **Sylwester 5 lat.**  
> **Biały 15 lat.**  
> **Biały 5 lat.**  
> **-------**  
> **4**  
> **-3**  
> **-3**  
> **1**  
> **3**  
> **-8**  
> **13**  
> **13**  
> **-4**  
> **-1**  
> **-12**  
> **-4**  
> **-12**  
> **0**  
> **Biały 5 lat.**  
> **Biały 15 lat.**  
> **Bury 5 lat.**  
> **Ciapek 5 lat.**  
> **Fajtłapa 10 lat.**  
> **Filemon 5 lat.**  
> **Sylwester 5 lat.**  

<br>

### Java Collections  

| Collection            | Interface   | Ordered | Sorted | Thread safe | Duplicate | Nullable       |
|-----------------------|-------------|---------|--------|-------------|-----------|----------------|
| ArrayList             | List        | Y       | N      | N           | Y         | Y              |
| Vector                | List        | Y       | N      | Y           | Y         | Y              |
| LinkedList            | List, Deque | Y       | N      | N           | Y         | Y              |
| CopyOnWriteArrayList  | List        | Y       | N      | Y           | Y         | Y              |
| HashSet               | Set         | N       | N      | N           | N         | One null       |
| LinkedHashSet         | Set         | Y       | N      | N           | N         | One null       |
| TreeSet               | Set         | Y       | Y      | N           | N         | N              |
| CopyOnWriteArraySet   | Set         | Y       | N      | Y           | N         | One null       |
| ConcurrentSkipListSet | Set         | Y       | Y      | Y           | N         | N              |
| HashMap               | Map         | N       | N      | N           | N (key)   | One null (key) |
| HashTable             | Map         | N       | N      | Y           | N (key)   | N (key)        |
| LinkedHashMap         | Map         | Y       | N      | N           | N (key)   | One null (key) |
| TreeMap               | Map         | Y       | Y      | N           | N (key)   | N (key)        |
| ConcurrentHashMap     | Map         | N       | N      | Y           | N (key)   | N              |
| ConcurrentSkipListMap | Map         | Y       | Y      | Y           | N (key)   | N              |
| ArrayDeque            | Deque       | Y       | N      | N           | Y         | N              |
| PriorityQueue         | Queue       | Y       | N      | N           | Y         | N              |
| ConcurrentLinkedQueue | Queue       | Y       | N      | Y           | Y         | N              |
| ConcurrentLinkedDeque | Deque       | Y       | N      | Y           | Y         | N              |
| ArrayBlockingQueue    | Queue       | Y       | N      | Y           | Y         | N              |
| LinkedBlockingDeque   | Deque       | Y       | N      | Y           | Y         | N              |
| PriorityBlockingQueue | Queue       | Y       | N      | Y           | Y         | N              |

LEGENDA:  
- Interface: Interfejs, który implementuje dany typ kolekcji.
- Ordered: Czy elementy są przechowywane w kolejności w jakiej zostały dodane.
- Sorted: Czy elementy są przechowywane w uporządkowany sposób, zgodnie z ich naturalnym porządkiem.
- Thread safe: Czy kolekcja jest bezpieczna do użycia w wielowątkowym środowisku.
- Duplicate: Czy kolekcja może zawierać duplikaty elementów.
- Nullable: Czy kolekcja może zawierać wartości null.  

Wartości:
- Y: Tak
- N: Nie
- One null: Może zawierać tylko jeden null \(w przypadku HashSet, HashMap itp.\).
- One null \(key\): Może zawierać tylko jeden null jako klucz.  





