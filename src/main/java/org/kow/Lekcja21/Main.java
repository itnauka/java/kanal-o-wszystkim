//21. Kurs Java dla początkujących - Kolekcje: operacje, sortowanie
//https://www.youtube.com/watch?v=E4z0TtaEiGs&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=22

package org.kow.Lekcja21;

import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        //Lista typu generycznego
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Poznań");
        lista.add("Kraków");
        lista.add("Wrocław");
        lista.add("Nowy Sącz");
        lista.add("Limanowa");
        lista.add("Szczecin");

        for (String miasto : lista) {
            System.out.println(miasto);
        }

        System.out.println("-------");

        Collections.sort(lista);

        for (String miasto : lista) {
            System.out.println(miasto);
        }

        System.out.println("-------");

        System.out.println(Collections.min(lista));

        System.out.println("-------");

        System.out.println(Collections.max(lista));

        System.out.println("-------");

        Collections.reverse(lista);
        for (String miasto : lista) {
            System.out.println(miasto);
        }

        System.out.println("-------");

        Collections.shuffle(lista);
        for (String miasto : lista) {
            System.out.println(miasto);
        }

        System.out.println("==++++++++==");

        ArrayList<Animal> koty = new ArrayList<>();
        Animal kot1 = new Animal("Bury");
        Animal kot2 = new Animal("Filemon");
        Animal kot3 = new Animal("Ciapek");
        Animal kot4 = new Animal("Fajtłapa");
        Animal kot5 = new Animal("Sylwester");
        Animal kot6 = new Animal("Biały");
        Animal kot7 = new Animal("Biały");
        kot4.wiek = 10;
        kot6.wiek = 15;


        koty.add(kot1);
        koty.add(kot2);
        koty.add(kot3);
        koty.add(kot4);
        koty.add(kot5);
        koty.add(kot6);
        koty.add(kot7);

        for (Animal kot : koty) {
            System.out.println(kot.name + " " + kot.wiek + " lat.");
        }

        System.out.println("-------");

        Collections.sort(koty);
        for (Animal kot : koty) {
            System.out.println(kot.name + " " + kot.wiek + " lat.");
        }
    }
}
