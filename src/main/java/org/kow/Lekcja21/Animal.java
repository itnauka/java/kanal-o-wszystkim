package org.kow.Lekcja21;

public class Animal implements Comparable<Animal>{
    String name;
    int wiek = 5;

    Animal(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Animal o) {
        System.out.println(this.name.compareTo(o.name));
        if (this.name.compareTo(o.name) != 0) {
            return this.name.compareTo(o.name);
        }
        return this.wiek - o.wiek;
    }
}
