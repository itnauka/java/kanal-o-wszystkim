<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#8 - Piszemy prostą grę](https://www.youtube.com/watch?v=UGCuVGcCaoY&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=9)

```java
public class Main {
    public static void main(String[] args) {
        Random random = new Random();
/*        
        //Test - Random'owe 100 liczb
            for (int i = 0; i < 100; i++) {
                System.out.println(random.nextInt(1, 101));
            }
*/
        
        int liczbaLosowa = random.nextInt(1, 101);
        System.out.println("liczba wylosowana przez komputer to: " + liczbaLosowa);

        Scanner scanner = new Scanner(System.in);
        boolean wygrana = false;

        //Ilość nieudanych prób
        int proby = 0;

        do {
            proby++;
            if (proby == 1) {
                System.out.print("Podaj swoją liczbę ");
            } else
                System.out.print("Podaj liczbę raz jeszcze: ");
            int liczbaOdgadnieta = scanner.nextInt();

            if (liczbaLosowa > liczbaOdgadnieta) {
                System.out.println("Twoja liczba " + liczbaOdgadnieta + " jest za MAŁA");
            } else if (liczbaLosowa < liczbaOdgadnieta) {
                System.out.println("Twoja liczba " + liczbaOdgadnieta + " jest za DUŻA");
            } else {
                wygrana = true;
                System.out.println("BRAWOOO, gratulacje wygrałeś, twoja prawidłowa liczba to " + liczbaOdgadnieta);
            }
        } while (!wygrana);
        System.out.println("KONIEC GRY!!!");
        System.out.println("Ilość wszystkich nieudanych prób wynosi: " + (proby - 1));
    }
}
```  

> **liczba wylosowana przez komputer to: 62**  
> **Podaj swoją liczbę** <font color="green">**63**</font>  
> **Twoja liczba 63 jest za DUŻA**  
> **Podaj liczbę raz jeszcze:** <font color="green">**61**</font>  
> **Twoja liczba 61 jest za MAŁA**  
> **Podaj liczbę raz jeszcze:** <font color="green">**62**</font>  
> **BRAWOOO, gratulacje wygrałeś, twoja prawidłowa liczba to 62**  
> **KONIEC GRY!!!**  
> **Ilość wszystkich nieudanych prób wynosi: 2**  







