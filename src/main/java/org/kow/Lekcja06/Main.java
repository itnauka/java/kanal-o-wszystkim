//6. Kurs Java dla początkujących - Instrukcje Switch
//https://www.youtube.com/watch?v=-VvDEB8N1Uw&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=7

package org.kow.Lekcja06;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj numer od 1 do 4: ");
        int poraRoku = scanner.nextInt();
        switch (poraRoku) {
            case 1:
                System.out.println("Wiosna");
                break;
            case 2:
                System.out.println("Lato");
                break;
            case 3:
                System.out.println("Jesień");
                break;
            case 4:
                System.out.println("Zima");
                break;
            default:
                System.out.println("Zły numer");
        }

        switch (poraRoku) {
            case 1 -> System.out.println("pora 1");
            case 2 -> System.out.println("pora 2");
            case 3 -> System.out.println("pora 3");
            case 4 -> System.out.println("pora 4");
            default -> System.out.println("To nie jest dobra pora");
        }
    }
}
