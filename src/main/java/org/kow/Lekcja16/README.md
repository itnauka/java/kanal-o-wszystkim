<sub>[Powrót do spisu treści](kanal-o-wszystkim)<sub>  

## Kurs Java dla początkujących

### [#16 - Klasy i obiekty, kopiowanie](https://www.youtube.com/watch?v=t-OIW2qbrt4&list=PL6aekdNhY7DCM1wGLQCE9eP3kPzu-P7E7&index=17)

##### Klasa `Liczba`  

```java
public class Liczba {
    int liczba;
}
```

<br>

##### Klasa `Main`

###### Typy proste

```java
public class Main {
    public static void main(String[] args) {
        
        //Typy proste
        int liczba = 50;
        int liczba2 = liczba;
        liczba2 = 111;
        System.out.println("Liczba przed: " + liczba);
        zmien(liczba);
        System.out.println("Liczba po: " + liczba);
    }
    
    public static void zmien(int licz) {//Kopiowanie wartości
        licz = -1;
        System.out.println("Liczba w metodzie zmień() to : " + licz);
    }
    
    public static void zmien(Liczba oLicz) {//Kopiowanie referencji
        oLicz.liczba = -1;
        System.out.println("Liczba w metodzie zmień() to : " + oLicz.liczba);
    }
```  

> **Liczba przed: 50**  
> **Liczba w metodzie zmień() to : -1**  
> **Liczba po: 50**  


<br>

###### Typy złożone

```java
public class Main {
    public static void main(String[] args) {
        
        //Typy złożone
        Liczba oLiczba = new Liczba();
        oLiczba.liczba = 50;
        Liczba oliczba2 = oLiczba;//Kopiowanie referensji (wskaźnika)
        oliczba2.liczba = 111;
        System.out.println("Obiekt liczba przed: " + oLiczba.liczba);
        zmien(oLiczba);
        System.out.println("Obiekt liczba po: " + oLiczba.liczba);
    }
    
    public static void zmien(int licz) {//Kopiowanie wartości
        licz = -1;
        System.out.println("Liczba w metodzie zmień() to : " + licz);
    }
    
    public static void zmien(Liczba oLicz) {//Kopiowanie referencji
        oLicz.liczba = -1;
        System.out.println("Liczba w metodzie zmień() to : " + oLicz.liczba);
    }
```  

> **Obiekt liczba przed: 111**  
> **Liczba w metodzie zmień() to : -1**  
> **Obiekt liczba po: -1**  

<br>





